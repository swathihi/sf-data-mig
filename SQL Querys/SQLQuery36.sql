SELECT  *
FROM [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp 
INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999'  and wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN [dbo].[src_Actions] a ON wccp.[Employee_Num]= a.[Personnel number]  and a.[End Date] != '12/31/9999' 
Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number 
INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]
INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason
INNER JOIN [dbo].[dest_PersonalArea_M] pa on ass.Personnel_Area = pa.PA
INNER JOIN [dbo].[dest_PersonalSubarea_M] psa on ass.Personnel_Area = psa.PA and ass.Personnel_Subarea = psa.PSubarea
INNER JOIN [dbo].[dest_EmployeeGroup_M] eg on eg.EmployeeGroup = ass.Employee_Group
INNER JOIN [dbo].dest_EmployeeSubgroup_M esg on esg.EmployeeSubGroup = ass.Employee_Subgroup
INNER Join [dbo].[src_AcquiredCompany_Info] aq on aq.[Personnel_number] = a.[Personnel number] 
INNER Join [dbo].[src_employees_tagging_report] etr on etr.[EMP_NO] = a.[Personnel number] 
LEFT JOIN [dbo].[src_Resignation_info] r on r.PersNo = a.[Personnel number]
