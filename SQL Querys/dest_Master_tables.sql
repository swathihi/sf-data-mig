Use [SF_Datamigration_Final]

CREATE TABLE [dbo].[dest_Title_M](
	Id int Identity (0,1) primary key,
	[Title] [nvarchar](255) NOT NULL
)
GO


CREATE TABLE [dbo].[dest_MaritalStatus_M](
	Id int Identity (0,1) primary key,
	[Marital Status] [nvarchar](255) NOT NULL)
GO



GO
CREATE TABLE [dbo].[dest_Gender_M](
	[GenderKey] nvarchar(1) NOT NULL primary key,
	[Description] [nvarchar](25) NULL,
)

CREATE TABLE [dbo].[dest_Country_M](
	[Country] [nvarchar](255) NOT NULL,
	[Nationality] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Country] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]


CREATE TABLE [dbo].[dest_EmploymentStatus_M](
	[EmploymentStatus] [int] Not NULL primary key,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]


CREATE TABLE [dbo].[dest_Actions_M](
	[Action] [nvarchar](255) NOT NULL,
	[NameOfActionType] [nvarchar](255) NOT NULL,
	Primary Key([Action])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[dest_PersonalArea_M](
	[PA] [nvarchar](255) NOT NULL,
	[CGrpg] [nvarchar](255) NULL,
	[CoCd] [nvarchar](255) NULL,
	[Personnel Area Text] [nvarchar](255) NULL,
	[Name 2] [nvarchar](255) NULL,
	[Ctr] [nvarchar](255) NULL,
	[Rg] [nvarchar](255) NULL,
	[Address] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[PA] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[dest_PersonalSubarea_M](
	[PA] [nvarchar](25) NOT NULL,
	[PSubarea] [nvarchar](255) NOT NULL,
	[PSubareaText] [nvarchar](255) NOT NULL,
	Constraint PK_PA_PSubarea Primary Key  ([PA],[PSubarea])
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[dest_EmployeeSubgroup_M]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_EmployeeSubgroup_M](
	[EmployeeGroup] [nvarchar](12) NOT NULL,
	[Description] [nvarchar](255) NULL,
	Primary Key ([EmployeeGroup])
) ON [PRIMARY]
GO


SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_EmployeeSubgroup_M](
	[EmployeeSubGroup] [nvarchar](12) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	Primary Key ([EmployeeSubGroup])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[dest_CountryRegionCode_M](
	[Country] [nvarchar](25) NOT NULL,
	[RegionCode] [nvarchar](255) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	Constraint PK_Country_RegionCode Primary Key  ([Country],[RegionCode])
) ON [PRIMARY]

CREATE TABLE [dbo].[dest_CountryGrouping_M](
	[CountryGrouping] [int] NOT NULL,
	[Description] [nvarchar](255) NULL,
	Primary Key ([CountryGrouping])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[dest_PayScaleType_M](
	[PayScaleType] [int] NOT NULL,
	[Description] [nvarchar](255) NULL
	Primary Key ([PayScaleType])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[dest_WageType_M](
	[CountryGroup] [int] NOT NULL,
	[WageType] [nvarchar](25) NOT NULL,
	[EndDate] [nvarchar](255) NOT NULL,
	[StartDate] [nvarchar](255) NULL,
	[Longtext] [nvarchar](255) NULL,
	[WTShortText] [nvarchar](255) NULL,
	Constraint PK_Wagetype_Enddate Primary Key  ([WageType],[EndDate])
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[dest_Address_Subtype_M](
	[SubType] [int] NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[SubType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[dest_BankDetailsType_M](
	[BankDetailsType] [int] NOT NULL,
	[Descrption] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[BankDetailsType] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[dest_PaymentMethods_M](
	[PaymentMethod] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[PaymentMethod] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO


CREATE TABLE [dbo].[dest_Child_M](
	[Id] [int] NOT NULL Primary Key ,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[dest_TypeOfFamilyRecord_M](
	[TypeOfFamilyRecord] [int] Not NULL primary key,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[dest_Communication_SubTypes_M](
	[Subtypes] [nvarchar](25) NOT NULL primary key,
	[Description] [nvarchar](255) NOT NULL
) ON [PRIMARY]
GO

--Need primary key for this table
CREATE TABLE [dbo].[dest_Leaves_Type_M](
	[AORAType] [nvarchar](25) NOT NULL,
	[EndDate] [datetime] NOT NULL,
	[StartDate] [datetime] NULL,
	[AttORABSTypeText] [nvarchar](255) NULL,
	--Constraint PK_AORAType_EndDate Primary Key  ([AORAType],[EndDate])
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[dest_AdditionalPayments_M](
	[ReasonForChangingMasterData] [nvarchar](25) NOT NULL,
	[Description] [nvarchar](255) NOT NULL,
	Primary Key ([ReasonForChangingMasterData])
) ON [PRIMARY]
GO
----------------------------------------------------------------------------------------





/****** Object:  Table [dbo].[ActionsM_AR]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[dest_ActionReasons_M](
	[Action] [nvarchar](255) NOT NULL,
	[ActReason] [nvarchar](255) NOT NULL,
	[NameOfReasonForAction] [nvarchar](255) NOT NULL
	Constraint PK_Action_ActReason Primary Key  ([Action],[ActReason])
) ON [PRIMARY]
GO



/****** Object:  Table [dbo].[Active_empPF]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Active_empPF](
	[PersNo] [nvarchar](255) NULL,
	[End Date] [datetime] NULL,
	[Start Date] [datetime] NULL,
	[Chngd On] [datetime] NULL,
	[Changed by] [nvarchar](255) NULL,
	[Ee PF] [nvarchar](255) NULL,
	[Eepnn] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Additional_Payments]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Additional_Payments](
	[PersNo] [nvarchar](255) NULL,
	[End Date] [datetime] NULL,
	[Start Date] [datetime] NULL,
	[Chngd On] [datetime] NULL,
	[Changed by] [nvarchar](255) NULL,
	[Re] [nvarchar](255) NULL,
	[Wage Type] [nvarchar](255) NULL,
	[Amount] [float] NULL,
	[Crcy] [nvarchar](255) NULL,
	[Origin dt#] [datetime] NULL,
	[Payment Date] [datetime] NULL,
	[Amount1] [float] NULL,
	[Crcy1] [nvarchar](255) NULL,
	[Remarks] [nvarchar](255) NULL,
	[Recovery Period in Months] [nvarchar](255) NULL,
	[Sponsorship] [nvarchar](255) NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Address]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Address](
	[PersNo] [int] NOT NULL,
	[End_Date] [datetime2](7) NOT NULL,
	[Start_Date] [datetime2](7) NOT NULL,
	[Chngd_On] [datetime2](7) NOT NULL,
	[Changed_by] [nvarchar](50) NULL,
	[Type] [nvarchar](50) NOT NULL,
	[Care_Of_Name] [nvarchar](50) NULL,
	[Street_and_House_Number] [nvarchar](100) NULL,
	[City] [nvarchar](50) NULL,
	[District] [nvarchar](50) NULL,
	[Postal_code] [nvarchar](50) NULL,
	[Ctr] [nvarchar](50) NOT NULL,
	[Telephone_no] [nvarchar](50) NULL,
	[_2nd_address_line] [nvarchar](50) NULL,
	[Rg] [nvarchar](50) NULL,
	[House] [nvarchar](50) NULL,
	[Aptmt] [nvarchar](50) NULL
) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Adhar_Numbers]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Adhar_Numbers](
	[Personnel number] [nvarchar](255) NULL,
	[End Date] [datetime] NULL,
	[Start Date] [datetime] NULL,
	[Chngd On] [datetime] NULL,
	[Changed by] [nvarchar](255) NULL,
	[Adhar Number] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Assignment]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Assignment](
	[Personnel_number] [int] NOT NULL,
	[End_Date] [datetime2](7) NOT NULL,
	[Start_Date] [datetime2](7) NOT NULL,
	[Chngd_On] [datetime2](7) NOT NULL,
	[Changed_by] [nvarchar](50) NOT NULL,
	[Company_Code] [nvarchar](50) NOT NULL,
	[Personnel_Area] [nvarchar](50) NOT NULL,
	[Employee_Group] [nvarchar](50) NULL,
	[Employee_Subgroup] [nvarchar](50) NOT NULL,
	[Organizational_Key] [nvarchar](50) NULL,
	[Business_Area] [nvarchar](50) NULL,
	[Personnel_Subarea] [nvarchar](50) NOT NULL,
	[Payroll_Area] [nvarchar](50) NOT NULL,
	[Cost_Center] [nvarchar](50) NULL,
	[Organizational_Unit] [nvarchar](50) NOT NULL,
	[Position] [nvarchar](50) NOT NULL,
	[Job] [nvarchar](50) NOT NULL,
	[Employee_s_Name] [nvarchar](50) NOT NULL,
	[Name_of_Employee_or_Applicant] [nvarchar](50) NOT NULL,
	[Administrator_Group] [nvarchar](50) NOT NULL,
	[Controlling_Area] [nvarchar](50) NULL
) ON [PRIMARY]
GO




	

/****** Object:  Table [dbo].[Details2M_CG]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Details2M_CG](
	[Country Group] [float] NULL,
	[Wage Type] [nvarchar](255) NULL,
	[End Date] [nvarchar](255) NULL,
	[Start Date] [nvarchar](255) NULL,
	[Long text] [nvarchar](255) NULL,
	[WT Short Text] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Details2M_Desp]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Details2M_Desp](
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Details2M_PA]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Details2M_PA](
	[Payscale Area] [nvarchar](255) NULL,
	[Description] [nvarchar](255) NULL
) ON [PRIMARY]
GO




GO
/****** Object:  Table [dbo].[PersonalDataM_Gender]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonalDataM_Gender](
	[Gender] [nvarchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Gender] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recurring_PaymentsM]    Script Date: 9/29/2021 6:13:19 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recurring_PaymentsM](
	[Wage Types are in PA0008 sheet#] [nvarchar](255) NULL
) ON [PRIMARY]
GO
