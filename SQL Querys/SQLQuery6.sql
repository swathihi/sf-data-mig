SELECT *
 ,(SELECT Top 1 Code FROM [dbo].[master_EmployeeSubgroup] esg WHERE temp.EmployeeSubGrp = esg.Description) AS EmpSubGrpCode
FROM 
(
SELECT [Employee_Num]
      ,[Employee_Name]
      ,[Role]
      ,[column4]
      ,[Designation_text]
      ,[Supervisor_ID]
      ,[Supervisor_Name]
	  , CASE 
			WHEN [L0] = 'Amalner Factory'
				THEN '9200'
			WHEN [L0] = 'WEL Corporate'
				THEN 'WEL1'
			WHEN [L0] = 'Wipro Consumer Care'
				THEN  '1000'
	   END AS CompanyCode
      ,[L0] AS CompanyName
	  ,(SELECT Top 1 Business_Unit_Code FROM [dbo].[master_OrgStructureDetails] WHERE [L0] = Company_Name_Label AND L1 = Business_Unit_Name_Label) AS BusinessUnitCode
      ,[L1] AS BusinessUnit
	  ,(SELECT Top 1 Division_Code FROM [dbo].[master_OrgStructureDetails] WHERE [L0] = Company_Name_Label AND L1 = Business_Unit_Name_Label AND L2 = Division_Name_Label) AS DivisionCode
      ,[L2] AS Division
	  ,(SELECT Top 1 Department_Code FROM [dbo].[master_OrgStructureDetails] WHERE [L0] = Company_Name_Label AND L1 = Business_Unit_Name_Label AND L2 = Division_Name_Label AND L3 = Department_Name_Label) AS DepartmentCode
      ,[L3] AS Department
      ,[Location_Region]
	  ,CONCAT([PA],'-',[PSA]) AS PA_PSA_Name
	   ,(SELECT Top 1 Code_PA_PSA FROM [dbo].[master_Location] l WHERE CONCAT([PA],'-',[PSA]) = Location_Name AND 
				CASE 
			WHEN [L0] = 'Amalner Factory'
				THEN '9200'
			WHEN [L0] = 'WEL Corporate'
				THEN 'WEL1'
			WHEN [L0] = 'Wipro Consumer Care'
				THEN  '1000'
			END = CompanyCode
		) AS PA_PSA_Code
      ,[PA]
      ,[PSA]
      ,[Cost_Centre]
	   
     ,CASE [Emp_Subgrp]
		WHEN 'WCCLG Group SSG'
		  THEN ''
			WHEN 'WCCLG Group D1'
				THEN 'WCC D1'
			WHEN 'WCCLG Group A'
				THEN 'WCC A'
			WHEN 'RETAINER'
				THEN 'RETAINER'
			WHEN 'WCCLG Group F1'
				THEN 'WCC F1'
			WHEN 'WCCLG Group F2'
				THEN 'WCC F2'
			WHEN 'WCCLG Group B'
				THEN 'WCC B'
			WHEN 'WCCLG Group E'
				THEN 'WCC E'
			WHEN 'WCCLG Group D'
				THEN 'WCC D'
			WHEN 'WCCLG Group C'
				THEN 'WCC C'
			WHEN 'WCCLG Group B1'
				THEN 'WCC B1'
			WHEN 'WCCLG Group G1'
				THEN 'WCC G1'
			WHEN 'WCCLG Group G'
				THEN 'WCC G'
			WHEN 'WCCLG Group H'
				THEN 'WCC H'
			WHEN 'WCCLG Group G2'
				THEN 'WCC G2'
			--WHEN 'WCCLG Trainee'
			--	THEN 
			ELSE
				[Emp_Subgrp]			

		END AS EmployeeSubGrp
         ,[Emp_Subgrp]
      ,[HR_SAP_No]
      ,[HR]
  FROM [dbo].[src_Active Profile Mapping_2_11_2021] p
  )temp

WHERE emp_subgrp = 'WCCLG Group SSG'


