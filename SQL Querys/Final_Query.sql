
/*********************************1******************************/
-- Mapping sheet row #3 to #10
-- The information regarding Reporting Officer and Hr Manager is not mention.
-- Please not as of now we are considered only for active employees for first pass, Status is Hardcoded as 'Active'
-- Querys wrriten in WF01_WG01_Active_PA0002_Personal details_updated - Sheet with dest_Basic_User_Info_issue

INSERT INTO [dbo].[dest_Basic_User_Info]

           ([Status]

           ,[UserId]

           ,[UserName]

           ,[FirstName]

           ,[LastName]

           ,[ReportingOfficer]

           ,[HrManager]

           ,[Gender])

SELECT DISTINCT 'Active', p.[Personnel_number], c.[User ID/Mobile Number], p.First_name, p.Last_name, '', '', fmg.GenderKey
FROM [dbo].[src_Personal_data] p
INNER Join [dbo].[src_Communication] c on c.[Personnel number] = p.[Personnel_number] and c.SubType = '0001'

Inner Join [dbo].[dest_Gender_M] fmg on fmg.GenderKey = 
													CASE
														WHEN p.Gender_Key = 1 THEN 'M'
														WHEN p.Gender_Key = 2 THEN 'F'
													END
Where p.End_Date = '12/31/9999' and c.[End Date] = '12/31/9999'
--p.[Personnel_number] NOT IN ( 20073192, 14130815, 138784, 290385) 

-- we added c.Enddate since the records were duplicate with the userId in communication table chnaged for the user 14130081



-------------------------------------------------------------------------------------------------------------
/*********************************2******************************/
-- Mapping sheet row #12 to #16
-- Querys wrriten in WF01_WG01_Active_PA0002_Personal details_updated - Sheet with dest_Biographical_Info_issue
-- There are only 319 employeeIds  who has Country_of_Birth out of 6050 records 

INSERT INTO [dbo].[dest_Biographical_Info]

           ([UserId]

           ,[EmployeeId]

           ,[DateOfBirth]

           ,[CountryOrRegionOfBirth]
		   
		   ,[CountryOfOrigin])

SELECT  p.Personnel_number, p.Personnel_number, p.Date_of_Birth, p.Country_of_Birth, p.Nationality

FROM [dbo].[src_Personal_data] p
Where  p.End_Date = '12/31/9999' 
--Where p.Personnel_number IN ( 20073192, 14130815, 138784, 290385) 



-----------------------------------------------------------------------------------------------------------------
/*********************************3******************************/
-- Mapping sheet row #186 to #190
-- The information regarding Sequence Number and Pay Group is not mention.
-- Retrived only with the enddate of actions = '12/31/9999'

INSERT INTO [dbo].[dest_Comp_Info]

           ([UserId]

           ,[EventDate]

           ,[SequenceNumber]

           ,[EventReason]

           ,[PayGroup])

SELECT a.[Personnel number], a.[Start Date], '', a.[Reason For Action], ''

FROM [dbo].[src_Actions] a

Where  a.[End Date] = '12/31/9999'
--Where a.[Personnel number] IN ( 20073192, 14130815, 138784, 290385)

-----------------------------------------------------------------------------------------------------------------------
/*********************************4******************************/
-- Mapping sheet row #97 to #107

INSERT INTO [dbo].[dest_CSF_Addresses]

            ([PersonIdExternal],[EventDate],[AddressType],[CountryRegion],[HouseNumber],[Street] ,[ExtraAddressLine],[Pin],[City] ,[District] ,[State])

SELECT a.PersNo, a.[Start_Date], ad.[Description], c.Nationality, a.House, a.Street_and_House_Number, a._2nd_address_line, a.Postal_code, a.City, a.District, a.Rg
FROM [dbo].[src_Address] a
INNER Join [dbo].[dest_PersonalArea_M] pa on pa.Rg = a.Rg
INNER JOIN [dbo].[dest_Address_Subtype_M] ad on ad.SubType = a.Type
INNER JOIN [dbo].[dest_Country_M] c on c.Country = a.Ctr
WHERE a.End_Date = '12/31/9999' 
--Where a.PersNo IN ( 20073192, 14130815, 138784, 290385) 

-------------------------------------------------------------------------------------------------------------------------------------------------
/*********************************5******************************/
-- Revisit the entire query
-- Mapping sheet row #140 to #156
-- Assuming HR team will provide the default primary key value, For now given as 0.
-- The information regarding Related Person Id External, Country, National ID Card Type and National ID is not mention.
-- Hard Coded the National ID Card Type as 'Aadhar'

INSERT INTO [dbo].[dest_Dependent_Info]

          ([EventDate] ,[PersonIdExternal] ,[Relationship]  ,[Accompanying] ,[CopyAddressFromEmployee] ,[IsBeneficiary],[RelatedPersonIdExternal] ,[FirstName] ,[MiddleName] ,
		  [LastName],	[Gender]
	      ,[DateOfBirth] ,[CountryOfBirth],[Country],[NationalIDCardType]  ,[NationalID]  ,[IsPrimary])

SELECT fm.Start_Date, fm.PersNo, ft.[Description], 0, 0, 0, '', fm.First_name, ' ', fm.Last_name, fmg.GenderKey, fm.Birth_date, fm.[CoB], c.Nationality, '', '', 0

FROM [dbo].[src_Family_Members] fm

Inner Join [dbo].[dest_Gender_M] fmg on fmg.GenderKey = 
													CASE
														WHEN fm.Gen = 1 THEN 'M'
														WHEN fm.Gen = 2 THEN 'F'
													END
INNER JOIN [dbo].[dest_TypeOfFamilyRecord_M] ft on  ft.TypeOfFamilyRecord = fm.Membr
INNER JOIN [dbo].[dest_Country_M] c on c.Country = fm.Nat
WHERE fm.End_Date = '12/31/9999' 
-- Where a.[Personnel number] IN ( 20073192) and  fm.End_Date = '12/31/9999' and a.[End Date] = '12/31/9999'
--Where a.[Personnel number] IN ( 20073192, 14130815, 138784, 290385)

-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*********************************6******************************/

-- Mapping sheet row #158 to #161
-- Assuming HR team will provide the default primary key value, For now given as 0.

INSERT INTO [dbo].[dest_Email_Info]

           ([PersonIdExternal]

           ,[EmailType]

           ,[EmailAddress]

           ,[IsPrimary])

SELECT  c.[Personnel number], c.SubType, c.[Mail Id], CASE WHEN c.[Mail Id] = NULL THEN 0 ELSE 1 END

FROM [dbo].[src_Communication] c
WHERE c.SubType = '0010'

--Where c.[Personnel number] IN ( 20073192, 14130815, 138784, 290385) and c.SubType='0010'

---------------------------------------------------------------------------------------------------------------------------------------

-- Mapping sheet row #133 to #138
-- The information is not mention.
-- 20190513  this guy has contact number as text
INSERT INTO [dbo].[dest_Emergency_Contact]

           ([PersonIdExternal]

           ,[Name]

           ,[Relationship]

           ,[Phone]

           ,[EmergencyContact]

           ,[Primary])


SELECT [Employee_Number], [Emergency_Contact_Name], '', [Emergency_Contact_Number], '', ''
FROM [dbo].[src_EmergencyContactDetails]
WHERE [Employee_Number] NOT IN ( 20190513, 20190974)

--Where [PersonIdExternal] IN ( 20073192, 14130815, 138784, 290385)

----------------------------------------------------------------------------------------------------------------

-- Mapping sheet row #18 to #22
-- The information regarding Group Hire Date and Card Number is not mention.

INSERT INTO [dbo].[dest_Employment_Info-At_Hire]

           ([PersonIdExternal]

           ,[UserId]

           ,[HireDate]

           ,[GroupHireDate]

           ,[CardNumber])

SELECT tempEIAH.Personnel_number, tempEIAH.Personnel_number, tempEIAH.[Start Date], '', ''  FROM
(SELECT ROW_NUMBER() OVER(PARTITION BY p.Personnel_number ORDER BY a.[Action] ASC) AS [RowNumber],p.Personnel_number, a.[Start Date], a.[Action]
FROM [dbo].[src_Personal_data] p
Inner Join [dbo].[src_Actions] a on a.[Personnel number] = p.Personnel_number
WHERE a.[Action] = '01' OR a.[Action] = '03'
)tempEIAH WHERE tempEIAH.[RowNumber] = 1




--Where p.PersNo IN ( 20073192, 14130815, 138784, 290385)

-------------------------------------------------------------------------------------------------------------------

-- Mapping sheet row #163 to #171
-- The information regarding Reason For Exit, Payroll End Date, Ok To Rehire, Termination Reason and Regret Termination  is not mention.

INSERT INTO [dbo].[dest_Employment_Info-At_Term]
           ([UserId]
           ,[PersonIdExternal] 	
	       ,[SeparationDate] 	
	       ,[ReasonForExit] 
		   ,[ReasonForExit2]
		   ,[Notes]
	       ,[PayrollEndDate] 	
	       ,[LastDateWorked] 	
	       ,[OkToRehire] 	
	       ,[TerminationReason])
SELECT r.PersNo, r.PersNo2, r.Notice_Period_End_date, rrlf.ReasonForLeavingFirst,
rrls.ReasonForLeavingSecond, r.Comments,r.Actual_Relieving_Date, r.Actual_Relieving_Date,
CASE WHEN r.Resignation_impact = 'N' THEN 0 ELSE 1 END, ''
FROM [dbo].[src_Resignation_info] r
LEFT JOIN [dbo].[dest_resignation_reason_for_leaving_first_M] rrlf ON rrlf.Id = r.Reason_for_leaving_first
LEFT
JOIN [dbo].[dest_resignation_reason_for_leaving_second_M] rrls ON rrls.Id = r.Reason_for_leaving_Second

--Where ld.[Personnel number] IN ( 20073192, 14130815, 138784, 290385)


-----------------------------------------------------------------------------------------------------------------------------------------------

-- Revisit entire query
-- Mapping sheet row #24 to #66
-- need to write the probationary period end date logic

INSERT INTO [dbo].[dest_Job_Info]

           ([EventDate],[SeqNumber],[UserId] ,[EventReason],[Position],[Company],[BusinessUnit],[Division] ,[Department],[PaPsa] ,[GeoZoneOrRegion] ,[TimeZone] ,[CostCenter] ,
		   [Supervisor] ,[JobClassification] ,[Designation] ,[EmployeeGroup] ,[EmployeeSubGroup] ,[CompetitionClause],[ProbationaryPeriodEndDate] ,[PayScaleType] ,[PayScaleArea]
		   ,[PayScaleGroup] ,[PayScaleLevel] ,[EmployeeNoticePeriod],[EsiApplicable] ,[EmployeeWorkLocation] ,[OnSiteLocation] ,[LocationRegion] ,[Classification] ,
		   [HolidayCalendar] ,[WorkSchedule] ,[TimeProfile] ,[TimeRecordingProfile] ,[TimeRecordingAdmissibility],[TimeRecordingVariant])
SELECT a.[Start Date], 
		DENSE_RANK() OVER(ORDER BY a.[Start Date] ASC),
		a.[Personnel number], a.[Reason For Action], ass.Position, ass.Company_Code, ' ', ' ', ' ', CONCAT(ass.Personnel_Area, '-', ass.Personnel_Subarea) AS [PaPsa] , ' ', '',
		ass.Cost_Center, ' ', ass.Job,' ', ass.Employee_Group, ass.Employee_Subgroup, ' ', ' ', ' ', ' ', ' ', ' ', ass.Position, ' ', aq.[Acquired_Company_Details],
		' ', ' ', ' ', ' ', ' ', ' ', ' ',' ', ' '
FROM [dbo].[src_Actions] a


Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number 
INNER Join [dbo].[src_AcquiredCompany_Info] aq on aq.[Personnel_number] = a.[Personnel number]
LEFT JOIN [dbo].[src_Resignation_info] r on r.PersNo = a.[Personnel number]
--Where p.Personnel_number IN ( 20073192, 14130815, 138784, 290385)
WHERE (a.[Action] = '01' OR a.[Action] = '03') AND ass.End_Date = '12/31/9999' 
--Where p.Personnel_number IN ( 20073192, 14130815, 138784, 290385)
--order By a.[Personnel number], a.[Start Date] asc

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Mapping sheet row #181 to #184
-- The information is not mention.

INSERT INTO [dbo].[dest_Job_Relations_Info]

           ([UserId]

           ,[EventDate]

           ,[Relationship Type]

           ,[Name])

           
--SELECT '', '', '', ''
--FROM ***
--Where [UserId] IN ( 20073192, 14130815, 138784, 290385)

-----------------------------------------------------------------------------------------------------------------------------------------------

-- Mapping sheet row #90 to #95
-- The information regarding  [LinkedAdhaar] is not mention.
-- Hard Coded the values for [CountryOrRegion] as 'India' , [NationalIdCardType] as 'Aadhar' 
-- Assuming the default primary key value, For now given as 1.

INSERT INTO [dbo].[dest_National_ID]

           ([PersonIdExternal]

           ,[CountryOrRegion]
	
	       ,[NationalIdCardType]
	
	       ,[NationalId]
	
	       ,[IsPrimary]
	
	       ,[LinkedAdhaar])

SELECT  an.[Personnel number], 'India', 'Aadhar', an.[Adhar Number], 1, ''

FROM [dbo].[src_Aadhar_Numbers] an

--Where an.[Personnel number] IN ( 20073192, 14130815, 138784, 290385) 

------------------------------------------------------------------------------------------------------------------------
-- Mapping sheet row #109 to #120
-- The information regarding Routing Number is not mention.

INSERT INTO [dbo].[dest_Payment_Info]

           ([PaymentInformationWorker] 
	
	       ,[PaymentInformationEffectiveStartDate]
	
	       ,[CountryOrRegionOrCode]
	
	       ,[PayType]
	
	       ,[PaymentMethod]
	
	       ,[BankCountry]
	
	       ,[Bank]
	
	       ,[RoutingNumber]
	
	       ,[AccountOwner]
	
	       ,[AccountNumber]
	
	       ,[Currency])

SELECT  bd.[Personnel number], bd.[Start Date], bd.City, bdm.[Descrption], bdp.[Description], bd.[Bank Country Key], bd.[Bank Keys], '', bd.[Payee Name], 
		bd.[Bank account number], bd.[Payment Currency]
FROM    [dbo].[src_Bank_Details] bd
INNER JOIN [dbo].[dest_BankDetailsType_M] bdm on bdm.BankDetailsType = bd.[Bank Details Type]
INNER JOIN [dbo].[dest_PaymentMethods_M] bdp on bdp.PaymentMethod = bd.[Payment Method] 
WHERE bd.[End Date] = '12/31/9999' 
--Where bd.[Personnel number] IN ( 20073192, 14130815, 138784, 290385)

-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Mapping sheet row #68 to #87
-- The information regarding Native Preferred Language, Challenge Status, Certificate Start Date, Certificate End Date, Father Name and Blood Group is not mention.

INSERT INTO [dbo].[dest_Personal_Info]

          ([PersonIdExternal]
	       
		  ,[EventDate]
	
	      ,[FirstName] 
	
	      ,[LastName] 
	   
	      ,[MiddleName]
	
	      ,[Salutation] 
	
	      ,[DisplayName] 
	
	      ,[FormalName]
	 
	      ,[Gender] 
	
	      ,[MaritalStatus]
	
	      ,[MaritalStatusSince]

		  ,[NoOfChild]
	
	      ,[Nationality]
	
	      ,[SecondNationality] 
	
	      ,[NativePreferredLanguage] 
	
	      ,[ChallengeStatus] 
	
	      ,[CertificateStartDate]
	
	      ,[CertificateEndDate] 
	      
		  ,[FatherName]
	
	      ,[BloodGroup])

SELECT p.Personnel_number, p.[Start_Date], p.First_Name, p.Last_name, p.Middle_Name, t.Title, ' ', ' ',g.GenderKey, m.MaritalStatus, 
p.Valid_From_Date_of_Current_Marital_Status, 
p.Number_of_Children, p.Nationality, p.Second_Nationality, p.Mother_Tongue, chall.CGr, chall.[Start_Date], chall.End_Date, concat(depn.FirstName, ' ', depn.LastName), p.Blood_group
FROM [dbo].[src_Personal_data] p
LEFT Join [dbo].[dest_Title_M] t on t.Id = p.Title 
LEFT Join [dbo].[dest_Gender_M] g on g.GenderKey = 
													CASE
														WHEN p.Gender_Key = 1 THEN 'M'
														WHEN p.Gender_Key = 2 THEN 'F'
													END
LEFT Join [dbo].[dest_Country_M] c on c.Country = p.Nationality
LEFT Join [dbo].[dest_MaritalStatus_M] m on m.Id = p.Marital_Status_Key
LEFT JOIN [dbo].[src_challenge_Info] chall on chall.PersNo = p.Personnel_number
LEFT JOIN [dbo].[dest_Dependent_Info] depn on depn.PersonIdExternal = p.Personnel_number and depn.Relationship = 'Father'
WHERE p.End_Date = '12/31/9999'  
order by p.Personnel_number, p.Valid_From_Date_of_Current_Marital_Status desc
--Where p.PersNo IN ( 20073192, 14130815, 138784, 290385)



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Mapping sheet row #173 to #179
-- Assuming HR team will provide the default primary key value, For now given as 0.
-- The information regarding [CountryRegionCode], [AreaCode] is not available from source
-- The information regarding [Extension] is included as part of phonenumber



INSERT INTO [dbo].[dest_Phone_Info]

           ([PersonIdExternal]
	
	       ,[PhoneType]
	
	       ,[CountryRegionCode]
	
	       ,[AreaCode]
	
	       ,[PhoneNumber]
	
	       ,[Extension]
	
	       ,[IsPrimary])

SELECT  c.[Personnel number], c.SubType, '', '', c.[User ID/Mobile Number], '', 0

FROM    [dbo].[src_Communication] c

WHERE c.SubType='MPHN'

--Where c.[Personnel number] IN ( 20073192, 14130815, 138784, 290385) 

-- Mapping sheet row #200 to #205
-- The information is not mention.

INSERT INTO [dbo].[dest_Pay_Component_Non_Recurring]

          ([UserId]

          ,[Amount]
	
	      ,[IssueDate] 
	
	      ,[PayComponent] 
	
	      ,[Currency]
	
	      ,[SequenceNumber])

--SELECT '', '', '', '', '', ''
--FROM ***
--Where [UserId] IN ( 20073192, 14130815, 138784, 290385)

-------------------------------------------------------------------------------------------------------------------------

-- Mapping sheet row #192 to #198
-- The information is not mention.

INSERT INTO [dbo].[dest_Pay_Component_Recurring]

           ([UserId]
	
	       ,[EventDate] 
	
	       ,[SequenceNumber] 
	
	       ,[PayComponent] 
	
	       ,[Amount] 
	
	       ,[Currency]
		   ,[Frequency])

--SELECT '', '', '', '','', '',''
--FROM ***
--Where [UserId] IN ( 20073192, 14130815, 138784, 290385)
           

-------------------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- Mapping sheet row #122 to #131
-- The information is not mention.

INSERT INTO [dbo].[dest_Work_Permit]

           ([UserId]
	
	       ,[CountryOrRegion]
	
	       ,[DocumentType]
	
	       ,[DocumentTitle]
	
	       ,[DocumentNumber]
	
	       ,[IssueDate]
	
	       ,[IssuePlace]
	
	       ,[IssuingAuthority]
	
	       ,[ExpirationDate]
	
	       ,[Validated])

--SELECT '', '', '', '','', '', '', '','',''
--FROM [Work_Permit]
--Where [UserId] IN ( 20073192, 14130815, 138784, 290385)


-----------------------------------------------------------------------------------------------------------------------------------------------

-- Revisit entire query
-- Mapping sheet row #24 to #66
-- need to write the probationary period end date logic
--INSERT INTO [dbo].[dest_Job_Info_v2]
--           (
--		   [EventDate],[SeqNumber],[UserId],[Event],[EventReason],[CompanyCode],[BusinessUnit],[Division],[Department],[PaPsa],[CostCenter],[Manager],
--		   [EmployeeType],[EmployeeGroup],[Grade],[ProbationaryPeriodEndDate],[ContractEndDate],[EmployeeNoticePeriod],[Notes],[ChangedDate],[ChangedBy]
--		   )
SELECT  FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate], 
		ROW_NUMBER() OVER(PARTITION BY a.[Personnel number], a.[Start Date] ORDER BY a.[Start Date]  ASC) AS [SeqNumber],
		a.[Personnel number] AS [UserId], 
		concat(a.[Action],'-', am.NameOfActionType)AS [Event],
		concat(a.[Reason For Action], '-', arm.NameOfReasonForAction) AS [EventReason], 
		concat(ass.Company_Code,'-', '') AS [CompanyCode], 'Not Applicable' AS [BusinessUnit], 'Not Applicable' AS [Division], 
		concat(ass.[Organizational_Unit], '-', '') AS [Department],
		CONCAT(ass.Personnel_Area,'-', pa.[Personnel Area Text], '-', ass.Personnel_Subarea, '-', psa.PSubareaText) AS [PaPsa],
		CONCAT(ass.Cost_Center, '-', '') AS [CostCenter], etr.[SUPERVISOR_ID] AS [Manager], etr.[DESIGNATION] AS Designation,' ' AS [EmployeeType], 
		CONCAT(ass.Employee_Group, '-', eg.[Description]) AS [EmployeeGroup], 
		--a.[action] AS [action],
		CONCAT(ass.Employee_Subgroup, '-', esg.[Description]) AS [Grade], 
		CASE 
				WHEN ass.Employee_Group = '4' and a.[action] = '1'
					THEN FORMAT (DATEADD(year, 1, a.[Start Date]), 'yyyy-MM-dd')
				WHEN 
					ass.Employee_Group = '5' and a.[action] = '5' --Allow only India 
					THEN FORMAT (a.[Start Date], 'yyyy-MM-dd')
				ELSE
					' ' 
		END AS [ProbationaryPeriodEndDate],
		'' AS [ContractEndDate],
		CASE 
			WHEN ass.Company_Code = 'WF01'
				THEN					
					  CASE 
						WHEN ass.Employee_Subgroup IN ('96','97')
							THEN '4 months'
						WHEN ass.Employee_Subgroup IN ('91','92', '93', '94', '95') 
							THEN '3 months'
						WHEN ass.Employee_Subgroup IN ('88','89') 
							THEN '2 months'
						WHEN ass.Employee_Subgroup IN ('WG', '85','86','87') 
							THEN '1 month'
						ELSE
							'1 month'
					END		
			WHEN ass.Company_Code = 'WG01'
				THEN					
					=CASE 
						WHEN ass.Employee_Subgroup IN ('69', '70', '71', '72', '73', '75')
							THEN '2 months'
						WHEN ass.Employee_Subgroup IN ('62','63','64','65','66','67','68') 
							THEN '3 months'
						WHEN ass.Employee_Subgroup IN ('88','89') 
							THEN '2 months'
						WHEN ass.Employee_Subgroup IN ('WG', '85','86','87') 
							THEN '1 month'
						ELSE
							'1 month'
					END		
					  			 
		END AS [EmployeeNoticePeriod],
		--FORMAT (r.[Notice_Period_End_date], 'yyyy-MM-dd') AS [EmployeeNoticePeriod],
		'' AS [Notes],
		FORMAT (a.[Chngd On], 'yyyy-MM-dd') AS [ChangedDate], a.[Changed by] as [ChangedBy]
FROM [dbo].[src_Actions] a
Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number 
INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]
INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason
INNER JOIN [dbo].[dest_PersonalArea_M] pa on ass.Personnel_Area = pa.PA
INNER JOIN [dbo].[dest_PersonalSubarea_M] psa on ass.Personnel_Area = psa.PA and ass.Personnel_Subarea = psa.PSubarea
INNER JOIN [dbo].[dest_EmployeeGroup_M] eg on eg.EmployeeGroup = ass.Employee_Group
INNER JOIN [dbo].dest_EmployeeSubgroup_M esg on esg.EmployeeSubGroup = ass.Employee_Subgroup
INNER Join [dbo].[src_AcquiredCompany_Info] aq on aq.[Personnel_number] = a.[Personnel number] 
INNER Join [dbo].[src_employees_tagging_report] etr on etr.[EMP_NO] = a.[Personnel number] 
LEFT JOIN [dbo].[src_Resignation_info] r on r.PersNo = a.[Personnel number]


--Where p.Personnel_number IN ( 20073192, 14130815, 138784, 290385)
--WHERE (a.[Action] = '01' OR a.[Action] = '03') AND ass.End_Date = '12/31/9999' 
--Where p.Personnel_number IN ( 20073192, 14130815, 138784, 290385)


--order By [Personnel number], a.[Start Date], a.[Action] asc

--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
