SELECT  FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate], 
		ROW_NUMBER() OVER(PARTITION BY a.[Personnel number], a.[Start Date] ORDER BY a.[Start Date]  ASC) AS [SeqNumber],
		a.[Personnel number] AS [UserId], 
		concat(a.[Action],'-', am.NameOfActionType)AS [Event],
		concat(a.[Reason For Action], '-', arm.NameOfReasonForAction) AS [EventReason], 
		concat(ass.Company_Code,'-', '') AS [CompanyCode], 'Not Applicable' AS [BusinessUnit], 'Not Applicable' AS [Division], 
		concat(ass.[Organizational_Unit], '-', '') AS [Department],
		CONCAT(ass.Personnel_Area,'-', pa.[Personnel Area Text], '-', ass.Personnel_Subarea, '-', psa.PSubareaText) AS [PaPsa],
		CONCAT(ass.Cost_Center, '-', '') AS [CostCenter], etr.[SUPERVISOR_ID] AS [Manager], etr.[DESIGNATION] AS Designation,' ' AS [EmployeeType], 
		CONCAT(ass.Employee_Group, '-', eg.[Description]) AS [EmployeeGroup], 
		--a.[action] AS [action],
		CONCAT(ass.Employee_Subgroup, '-', esg.[Description]) AS [Grade], 
		CASE 
				WHEN ass.Employee_Group = '4' and a.[action] = '1'
					THEN FORMAT (DATEADD(year, 1, a.[Start Date]), 'yyyy-MM-dd')
				WHEN 
					ass.Employee_Group = '5' and a.[action] = '5' --Allow only India 
					THEN FORMAT (a.[Start Date], 'yyyy-MM-dd')
				ELSE
					' ' 
		END AS [ProbationaryPeriodEndDate],
		'' AS [ContractEndDate],
		CASE WHEN ass.Company_Code = 'WG01'
				THEN					
					  CASE 
						WHEN ass.Employee_Subgroup IN ('69', '70', '71', '72', '73', '75')
							THEN '2 months'
						WHEN ass.Employee_Subgroup IN ('62','63','64','65','66','67','68') 
							THEN '3 months'
						WHEN ass.Employee_Subgroup IN ('88','89') 
							THEN '2 months'
						WHEN ass.Employee_Subgroup IN ('WG', '85','86','87') 
							THEN '1 month'
						ELSE
							'1 month'
					END		
					  			 
		END AS [EmployeeNoticePeriod],
		--FORMAT (r.[Notice_Period_End_date], 'yyyy-MM-dd') AS [EmployeeNoticePeriod],
		'' AS [Notes],
		FORMAT (a.[Chngd On], 'yyyy-MM-dd') AS [ChangedDate], a.[Changed by] as [ChangedBy]
FROM [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp 
INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999'  and wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN [dbo].[src_Actions] a ON wccp.[Employee_Num]= a.[Personnel number]  and a.[End Date] != '12/31/9999' 
Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number 
INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]
INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason
INNER JOIN [dbo].[dest_PersonalArea_M] pa on ass.Personnel_Area = pa.PA
INNER JOIN [dbo].[dest_PersonalSubarea_M] psa on ass.Personnel_Area = psa.PA and ass.Personnel_Subarea = psa.PSubarea
INNER JOIN [dbo].[dest_EmployeeGroup_M] eg on eg.EmployeeGroup = ass.Employee_Group
INNER JOIN [dbo].dest_EmployeeSubgroup_M esg on esg.EmployeeSubGroup = ass.Employee_Subgroup
INNER Join [dbo].[src_AcquiredCompany_Info] aq on aq.[Personnel_number] = a.[Personnel number] 
INNER Join [dbo].[src_employees_tagging_report] etr on etr.[EMP_NO] = a.[Personnel number] 
LEFT JOIN [dbo].[src_Resignation_info] r on r.PersNo = a.[Personnel number]
