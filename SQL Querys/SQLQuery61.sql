


UPDATE [src_Active Profile Mapping_28_10_21] SET L3 = 'Sales - Line 2'  WHERE L3 IN ('Sales - Line2')

UPDATE [src_Active Profile Mapping_28_10_21]  SET  PSA = 'Kualalumpur' WHERE PSA = 'Kuala Lumpur'

UPDATE [src_Active Profile Mapping_28_10_21] SET PSA = 'Blr Wipro House' WHERE PSA = 'Wipro House'


UPDATE [src_Active Profile Mapping_28_10_21] SET L2 = 'Amalner 1' WHERE L2 = 'Amalner'

UPDATE [src_Active Profile Mapping_28_10_21] SET L2 = 
			CASE WHEN PSA = 'Baddi - 1' OR PSA = 'Baddi-1'
				THEN 'Baddi CC-1 Plant 1'
				WHEN PSA = 'Baddi - 2'
				THEN 'Baddi CC-2'
				ELSE
					PSA
			END	
WHERE L2 = 'Baddi'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Designation_text] = REPLACE([Designation_text], 'Sr', 'Senior')
WHERE Designation_Text Like 'sr%'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Designation_text] = REPLACE([Designation_text], 'Senior.', 'Senior ')
WHERE Designation_Text Like 'Senior.%'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Designation_text] = REPLACE([Designation_text], 'Asst.', 'Assistant')
WHERE Designation_Text Like 'Asst.%'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Designation_text] = REPLACE([Designation_text], 'Jr', 'Junior')
WHERE Designation_Text Like 'Jr%'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Designation_text] = REPLACE([Designation_text], 'Accounts Execuive', 'Accounts Executive')
WHERE Designation_Text Like 'Accounts Execuive'

UPDATE [dbo].[master_JobRole_DesignationDetails]  SET Designation_Description = REPLACE(Designation_Description, 'Senior.', 'Senior')
WHERE Designation_Description Like 'Senior.%'

UPDATE [dbo].[master_JobRole_DesignationDetails]  SET Designation_Description = REPLACE(Designation_Description, 'Assistant  Manager -  Production', 'Assistant  Manager - Production')
WHERE Designation_Description Like 'Assistant  Manager -  Production'

UPDATE [dbo].[master_JobRole_DesignationDetails]  SET Designation_Description = 'Senior Executive - Stores'
WHERE Designation_Description = 'SeniorExecutive - Stores'

UPDATE [dbo].[master_JobRole_DesignationDetails]  SET Designation_Description = 'Manager - Production'
WHERE Designation_Description LIKE 'Manager%Production' AND Job_Role_Code = '1095' 

UPDATE [dbo].[master_JobRole_DesignationDetails]  SET Designation_Description = 'Assistant Manager - Maintenance'
WHERE Designation_Description LIKE 'Assistant  Manager - Maintenance' 

UPDATE [dbo].[master_JobRole_DesignationDetails]  SET Designation_Description = 'Senior Engineer Production'
WHERE Designation_Description LIKE 'Senior Engineer - Production' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Designation_text] = 'Assistant  Manager - Production'
WHERE Designation_Text Like 'Assistant%Production' AND [Role] LIKE 'Production Manager'


UPDATE [dbo].[master_JobRole_DesignationDetails]  SET Designation_Description = REPLACE(Designation_Description, 'Accounts Execuive', 'Accounts Executive')
WHERE Designation_Description Like 'Accounts Execuive%'


UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Designation_text] = REPLACE([Designation_text], 'Asst', 'Assistant')
WHERE Designation_Text Like 'Asst%'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Role] = REPLACE([Role], 'Controllership Mgr', 'Controllership Manager ')
WHERE [Role] Like 'Controllership Manager'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Role] = REPLACE([Role], 'Business Finance Mgr', 'Business Finance Manager')
WHERE [Role] Like 'Business Finance Mgr'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Role] = REPLACE([Designation_text], 'Senior  ', 'Senior')
WHERE [Designation_text] Like 'Senior  %'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Role] = REPLACE([Role], 'Finance Mgr', 'Finance Manager')
WHERE [Role] Like 'Finance Mgr'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Role] = REPLACE([Role], 'ASM', 'Area Sales Manager ')
WHERE [Role] Like 'ASM'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Role] = REPLACE([Role], 'QA Manager', 'Quality Manager')
WHERE [Role] Like 'QA Manager'

  
SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE [Role] LIKE 'ASM (B2B)'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]  SET [Role] = REPLACE([Role], 'ASM (B2B)', 'Area Sales Manager (B2B)')
WHERE [Role] Like 'ASM (B2B)'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L2 = 'Sales - Yardley'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Yardley - Sales' 
WHERE L2 = 'Sales - Yardley' 


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L2 = 'LSG' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Lighting Solutions Group'  
WHERE L2 = 'LSG' 


-------------------Division--------------------------

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L2 = 'FMG' AND L1 != 'Support' AND L0 != 'Wipro Consumer Care'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L1 = 'Support' , L0 = 'Wipro Consumer Care'
WHERE L2 = 'FMG' AND L1 != 'Support' AND L0 != 'Wipro Consumer Care'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L2 = 'Secreterial'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Secretarial' 
WHERE L2 = 'Secreterial' 


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L2 = 'COO Manufacturing' AND L1 != 'Factory' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L1 = 'Factory' 
WHERE L2 = 'COO Manufacturing' AND L1 != 'Factory' 


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Employee_Num = '15022896'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'BO'  
WHERE Employee_Num = '15022896'




SELECT * FROM [dbo].[master_OrgStructureDetails] WHERE Division_Name_Label = 'Secreterial'

UPDATE [dbo].[master_OrgStructureDetails]
SET Division_Name_Label = 'Secretarial' 
WHERE Division_Name_Label = 'Secreterial'

SELECT * FROM [dbo].[master_OrgStructureDetails] WHERE Division_Name_Label = 'Hyderabad' 


UPDATE [dbo].[master_OrgStructureDetails]
SET Division_Name_Label = 'Hyderabad Plant' 
WHERE Division_Name_Label = 'Hyderabad'




SELECT * FROM [dbo].[master_OrgStructureDetails] WHERE Division_Name_Label = 'QA' 


UPDATE [dbo].[master_OrgStructureDetails]
SET Division_Name_Label = 'Quality' 
WHERE Division_Name_Label = 'QA'




SELECT * FROM [dbo].[master_OrgStructureDetails] WHERE Division_Name_Label = 'LSG' 


UPDATE [dbo].[master_OrgStructureDetails]
SET Division_Name_Label = 'Lighting Solutions Group' 
WHERE Division_Name_Label = 'LSG'




-------------------------------------------------------------


--------------------Department--------------------
SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Secreterial'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Secretarial' 
WHERE L3 = 'Secreterial' 

--------------------------------------------------------------------


--------------------------------------Location-------------------------------
SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Pune-BO'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Pune BO'  
WHERE PSA = 'Pune-BO'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Mumbai-Lotus'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Mumbai Lotus'  
WHERE PSA = 'Mumbai-Lotus'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Kolkata-RO'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Kolkata RO'  
WHERE PSA = 'Kolkata-RO'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Bangalore -BG'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Bangalore BG'  
WHERE PSA = 'Bangalore -BG'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Aurangabad-1'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Aurangabad 1'  
WHERE PSA = 'Aurangabad-1'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Tirupati -AO'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Tirupati AO'  
WHERE PSA = 'Tirupati -AO'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Nagpur-1'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Nagpur 1'  
WHERE PSA = 'Nagpur-1'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Ahmedabad-1'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Ahmedabad 1'  
WHERE PSA = 'Ahmedabad-1'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Pune-RO'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Pune RO'  
WHERE PSA = 'Pune-RO'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Bhiwandi-1'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Bhiwandi 1'  
WHERE PSA = 'Bhiwandi-1'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE PSA = 'Pune - RO'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET PSA = 'Pune RO'  
WHERE PSA = 'Pune - RO'



-----------------------------------------------------------------------------


--------------------------------------Employee Subgroup---------------------------------------



SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Emp_Subgrp = 'Fixed Term Employees'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET Emp_Subgrp = 'FTE'  
WHERE Emp_Subgrp = 'Fixed Term Employees'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Emp_Subgrp = 'WIN Group G'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET Emp_Subgrp = 'WIN G'  
WHERE Emp_Subgrp = 'WIN Group G'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Emp_Subgrp = 'GROUP D1'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET Emp_Subgrp = 'WCC D1'  
WHERE Emp_Subgrp = 'GROUP D1'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Emp_Subgrp = 'WIN Group F'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET Emp_Subgrp = 'WIN F'  
WHERE Emp_Subgrp = 'WIN Group F'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Emp_Subgrp = 'GROUP E'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET Emp_Subgrp = 'WCC E'  
WHERE Emp_Subgrp = 'GROUP E'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Emp_Subgrp = 'GROUP D'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET Emp_Subgrp = 'WCC D'  
WHERE Emp_Subgrp = 'GROUP D'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Emp_Subgrp = 'GROUP C'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET Emp_Subgrp = 'WCC C'  
WHERE Emp_Subgrp = 'GROUP C'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Emp_Subgrp = 'WIN Group B'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET Emp_Subgrp = 'WIN B'  
WHERE Emp_Subgrp = 'WIN Group B'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE Emp_Subgrp = 'Comrcl/Indus Trainee'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET Emp_Subgrp = 'EO'  
WHERE Emp_Subgrp = 'Comrcl/Indus Trainee'

-----------------------------------------------------------------------------

------------------------------------Department-----------------------------------------

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'HEAD-FACTORY'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Head - Factory'  
WHERE L3 = 'HEAD-FACTORY'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Sales - Line 1'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Sales - Line 1'  
WHERE L3 = 'Sales - Line 1'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Sales' and L1 = 'Furniture' and L2 = 'Project Installation'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Project Installation'  
WHERE L3 = 'Sales' and L1 = 'Furniture' and L2 = 'Project Installation'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Sales' and L1 = 'C&I' and L2 = 'PMG'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'PMG'  
WHERE L3 = 'Sales' and L1 = 'C&I' and L2 = 'PMG'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Human Resource'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'HR'  
WHERE  L3 = 'Human Resource'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'BO' and L1 = 'Consumer Lighting' AND Designation_text Like '%Marketing%'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Marketing'  
WHERE  L3 = 'BO' and L1 = 'Consumer Lighting' AND Designation_text Like '%Marketing%'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'BO' and L1 = 'Consumer Lighting' AND Designation_text Like '%Vice president%'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Sales' , L3 = 'Sales'  
WHERE  L3 = 'BO' and L1 = 'Consumer Lighting' AND Designation_text Like '%Vice president%'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Stores' and L2 = 'Baddi'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Baddi CC-1 Plant 1'   
WHERE  L3 = 'Stores'and L2 = 'Baddi'



SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'PRODUCTION- HARIDWAR-FAGP' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Production-FAGP'   
WHERE L3 = 'PRODUCTION- HARIDWAR-FAGP' 

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Fatty Acid - BADDI' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Fatty Acid'   
WHERE L3 = 'Fatty Acid - BADDI' 


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Manufacturing-Baddi CC-Unit II' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Manufacturing'   
WHERE L3 = 'Manufacturing-Baddi CC-Unit II' 

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Production-Haridwar' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Production'   
WHERE L3 = 'Production-Haridwar' 


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Utility-Haridwar' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Utility'   
WHERE L3 = 'Utility-Haridwar' 


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Utility' AND L2 = 'Baddi CC-1 Plant 1'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Baddi CC-1 Plant 2'   
WHERE L3 = 'Utility' AND L2 = 'Baddi CC-1 Plant 1'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'MATERIAL WALUJ' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Material'   
WHERE L3 = 'MATERIAL WALUJ'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'HK/Maint./Admin' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'HK/Maintenance/Admin'   
WHERE L3 = 'HK/Maint./Admin' 


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Engineering/Maintenance'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Maintenance'   
WHERE L3 = 'Engineering/Maintenance'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Human Resource -Haridwar'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'HR'   
WHERE L3 = 'Human Resource -Haridwar'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'FABRIC SOFTNER'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Fabric Softener'   
WHERE L3 = 'FABRIC SOFTNER'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'HK/MAINT./ADMIN - NW HARIDWAR HUB'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Hub Operations'   , L3 = 'Hub Operations'
WHERE L3 = 'HK/MAINT./ADMIN - NW HARIDWAR HUB'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Production-Haridwar-Switches_EWD'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Production-Switches_EWD'
WHERE L3 = 'Production-Haridwar-Switches_EWD'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Production-Hand Sanitizer-Waluj'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Production Hand Sanitizer'
WHERE L3 = 'Production-Hand Sanitizer-Waluj'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Material Acceptance and distrubution dep'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Material Acceptance and distribution'
WHERE L3 = 'Material Acceptance and distrubution dep'



SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Sweetner-Baddi'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Sweetner'
WHERE L3 = 'Sweetner-Baddi'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'CSP- BADDI CC'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'CSP'
WHERE L3 = 'CSP- BADDI CC'




SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Quality Assurance-Haridwar'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Quality Assurance'
WHERE L3 = 'Quality Assurance-Haridwar'



SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Material-Haridwar'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Material'
WHERE L3 = 'Material-Haridwar'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Sourcing'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Sourcing'
WHERE L3 = 'Sourcing'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Finance-Haridwar'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Finance'
WHERE L3 = 'Finance-Haridwar'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Grid Rm/Power House'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Grid Repair & Maintenance/Power House'
WHERE L3 = 'Grid Rm/Power House'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'HARIDWAR SAFEWASH COST CENTRE'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'SafeWash'
WHERE L3 = 'HARIDWAR SAFEWASH COST CENTRE'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'CONTINUOUS SAPONIFICATION PLANT'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Continuous Saponification'
WHERE L3 = 'CONTINUOUS SAPONIFICATION PLANT'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Power House'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Baddi CC-2'
WHERE L3 = 'Power House'


SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'HARIDWAR GLUCOVITA COST CENTRE'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L3 = 'Glucovita'
WHERE L3 = 'HARIDWAR GLUCOVITA COST CENTRE'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Internal Audit + Risk & Compliance' and L2 = 'Controllership'

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Internal Audit + Risk & Compliance'
WHERE L3 = 'Internal Audit + Risk & Compliance' and L2 = 'Controllership'

SELECT * FROM [dbo].[src_Active Profile Mapping_28_10_21]
WHERE L3 = 'Toilet Soap Plant' 

UPDATE [dbo].[src_Active Profile Mapping_28_10_21]
SET L2 = 'Toilet Soap'
WHERE  L3 = 'Toilet Soap Plant' 



 

-----------------------------------------------------------------------------