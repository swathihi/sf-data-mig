SELECT ass.Employee_Group,  eg.[Description], a.[Employee_num], a.[Employee_Name], CONCAT(ass.Employee_Group, '-', eg.[Description]) AS 'Employee Group',* FROM [dbo].[src_wcc_personal_data] a
LEFT Join [dbo].[src_Assignment] ass on a.[Employee_num] = ass.Personnel_number and ass.End_Date= '12/31/9999' 
LEFT JOIN [dbo].[dest_EmployeeGroup_M] eg on eg.EmployeeGroup = ass.Employee_Group

-----------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------------------------



-----------------------------------------------------Basic_User_Info------------------------------------------------------------------------------------------

SELECT DISTINCT 'Active' AS [Status], p.[Personnel_number] AS [UserId], c.[User ID/Mobile Number]AS [UserName], p.First_name AS [FirstName], 
p.Last_name AS [LastName], wccp.Supervisor_ID AS [ReportingOfficer], wccp.HR_SAP_No AS [HrManager], fmg.GenderKey AS [Gender]
--FROM [dbo].[src_wcc_personal_data] wccp
FROM [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp
INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999'
INNER Join [dbo].[src_Communication] c on c.[Personnel number] = p.[Personnel_number] and c.SubType = '0001' and c.[End Date] = '12/31/9999'

Inner Join [dbo].[dest_Gender_M] fmg on fmg.GenderKey = 
													CASE
														WHEN p.Gender_Key = 1 THEN 'M'
														WHEN p.Gender_Key = 2 THEN 'F'
													END
where wccp.[Exists_in_Basic_user_Id] =1
--WHERE wccp.[Employee_Num] NOT IN (15013760,15021492,15023845,20132664,20100823,20146787,15022556,15023197,20136026,15022614,20082300,15020588,15013363,881610,15013335,15022119,
--20148432,15021536,15022569,15023205,15013819,14131938,15001540,15020176,15020790,20141249,15023186,15021609,15012906,15023900)

-----------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------[Biographical_Info]-----------------------------------------------------------------------------------
SELECT  p.Personnel_number AS [UserId], p.Personnel_number AS [EmployeeId], 
FORMAT(p.Date_of_Birth,'yyyy-MM-dd') AS [DateOfBirth],
Upper(SUBSTRING(c.[Name], 1, 3)) AS [CountryOrRegionOfBirth], 
c.[Name] AS [CountryOfOrigin]
--FROM [dbo].[src_wcc_personal_data] wccp
FROM [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp
INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999'
INNER JOIN [dbo].[dest_Country_M] c on c.Ctr = p.Nationality
where wccp.[Exists_in_Basic_user_Id] =1
--WHERE wccp.[Employee_Num] NOT IN (15013760,15021492,15023845,20132664,20100823,20146787,15022556,15023197,20136026,15022614,20082300,15020588,15013363,881610,15013335,15022119,
--20148432,15021536,15022569,15023205,15013819,14131938,15001540,15020176,15020790,20141249,15023186,15021609,15012906,15023900)
-----------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------Employment_Info-At_Hire-------------------------------------------------------------------------

SELECT [Person_ID_External], [Person_ID_External] AS [User_ID], FORMAT([Hire_Date],'yyyy-MM-dd') AS [Hire_Date], FORMAT([Hire_Date],'yyyy-MM-dd') AS [Group_Hire_Date],
[Card_Number]
FROM
(SELECT ROW_NUMBER() OVER(PARTITION BY wccp.[Employee_Num] ORDER BY a.[Action] ASC) AS [RowNumber],wccp.[Employee_Num] AS [Person_ID_External], 
a.[Start Date] AS [Hire_Date], [card].[Card] AS [Card_Number], a.[Action]
FROM [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp
Inner Join [dbo].[src_Actions] a on a.[Personnel number] = wccp.[Employee_Num]
LEFT JOIN [dbo].[src_WEL_Access_card_details] [card] on wccp.[Employee_Num] = [card].[Employee_Code]
WHERE wccp.[Exists_in_Basic_user_Id] =1 AND (a.[Action] = '01' OR a.[Action] = '02' OR a.[Action] = '03' OR a.[Action] = '72')
)temp WHERE temp.[RowNumber] = 1
----------------------------------------------------------------------------------------------------------------------------------------------


--------------------------------------------------------------05. Personal_Info---------------------------------------------------------------------------------
SELECT p.Personnel_number AS Person_ID_External, FORMAT( p.[Start_Date],'yyyy-MM-dd') AS Event_Date, p.First_Name, p.Last_name, p.Middle_Name, 
t.Title AS Salutation, ' ' AS [Display_Name], ' ' AS [Formal_Name],g.GenderKey AS Gender, COALESCE(m.MaritalStatus, 'Unknown') AS [Marital_Status], 
FORMAT( p.Valid_From_Date_of_Current_Marital_Status,'yyyy-MM-dd') AS [Marital_Status_Since], 
p.Number_of_Children, 
Upper(SUBSTRING(c.[Name], 1, 3)) AS [Nationality] , 
p.Second_Nationality, p.Mother_Tongue AS [Native_Preferred_Language], 
CASE 
	WHEN chall.CGr IS NULL
		THEN 'No'
	WHEN chall.CGr = '08'
	THEN 'No'
	ELSE
	'	Yes'
 END AS [Challenge_Status], 
--FORMAT(chall.[Start_Date],'yyyy-MM-dd') AS [Certificate_Start_Date], FORMAT(chall.End_Date,'yyyy-MM-dd') AS [Certificate_End_Date], 
NULL AS [Certificate_Start_Date], NULL AS [Certificate_End_Date],
concat(depn.FirstName, ' ', depn.LastName) AS [Father_Name], 
	CASE
		WHEN  blg.[Blood Group] IS NOT NULL AND blg.[Blood Group] LIKE '%+%'
			THEN  LEFT(blg.[Blood Group], (CHARINDEX('+', blg.[Blood Group]) - 1)) + '-Positive'
		WHEN  blg.[Blood Group] IS NOT NULL AND blg.[Blood Group] LIKE '%-%'
			THEN LEFT(blg.[Blood Group], (CHARINDEX('-', blg.[Blood Group]) - 1)) + '-Negative'
	END AS [Blood Group]

FROM [dbo].[src_Personal_data] p
INNER JOIN [dbo].[src_active_17_11_2021] wccp ON p.Personnel_number = wccp.[Employee_Num] and p.End_Date = '12/31/9999' 
LEFT Join [dbo].[dest_Title_M] t on t.Id = p.Title 
LEFT Join [dbo].[dest_Gender_M] g on g.GenderKey = 
													CASE
														WHEN p.Gender_Key = 1 THEN 'M'
														WHEN p.Gender_Key = 2 THEN 'F'
													END
LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = p.Nationality
LEFT Join [dbo].[dest_MaritalStatus_M] m on m.Id = p.Marital_Status_Key
LEFT JOIN [dbo].[src_challenge_Info] chall on chall.PersNo = p.Personnel_number
LEFT JOIN [dbo].[dest_Dependent_Info] depn on depn.PersonIdExternal = p.Personnel_number and depn.Relationship = 'Father'
LEFT JOIN [dbo].[dest_BloodGroup_M] blg ON blg.Code = p.Blood_group
order by p.Personnel_number, p.Valid_From_Date_of_Current_Marital_Status desc
-----------------------------------------------------------------------------------------------------------------------------------------------


---------------------------------------------------------------06. National_Id--------------------------------------------------------------------------------
SELECT  wccp.[Employee_Num] AS [PersonIdExternal], Upper(SUBSTRING(c.[Name], 1, 3)) AS [CountryOrRegion], 'ADN' AS [NationalIdCardType],
		an.[Aadhar Number] AS [NationalId], 'Yes' AS [IsPrimary], '' AS [LinkedAdhaar]
FROM [dbo].[src_active_17_11_2021] wccp
LEFT JOIN [dbo].[WCC_EmployeeDataBase_Aadhar_Numbers] an ON wccp.[Employee_Num]   = an.[Employee Number (SAP Number - Eight Digit)] 
INNER JOIN [dbo].[dest_Country_M] c on c.[Name] = 'India'
WHERE wccp.[Employee_Num]   NOT IN (
'20181514',
'20162414',
'15000986',
'20174268',
'20184786',
'20177612',
'20180457',
'20190501',
'20173580',
'20160868',
'881407',
'20174555',
'20190513',
'20162908',
'20193762',
'20177613',
'20015354',
'20160694',
'20171138',
'20174269',
'20039282',
'546656',
'20190441',
'20175763',
'20157276',
'914129',
'20189661'
)
-----------------------------------------------------------------------------------------------------------------------------------------------



-------------------------------------------------------------07. CSF-Addresses----------------------------------------------------------------------------------

--SELECT a.PersNo AS [PersonIdExternal], 
--FORMAT( a.[Start_Date],'yyyy-MM-dd')  AS [EventDate], 
--CASE
--		WHEN ad.[SubType] = 1
--			THEN 'Present'
--		WHEN ad.[SubType] = 2
--			THEN 'Permanent'
--		ELSE	
--			ad.[Description]
--END AS [AddressType], Upper(SUBSTRING(c.[Name], 1, 3)) AS [CountryRegion],
--CASE WHEN a.House IS NOT NULL
--	THEN Concat(a.House, ' , ', a.Street_and_House_Number)
--	ELSE 
--		 a.Street_and_House_Number
--END AS [HouseNumber],
--' '  AS [Street], a._2nd_address_line AS [ExtraAddressLine], COALESCE(a.Postal_code, '0') AS [Pin], a.City AS [City], COALESCE(a.District, 'Not Applicable') AS [District],
--pa.[Personnel Area Text],
--COALESCE(ps.[Code], 'Not Applicable') AS [State]
--FROM [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp
--INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999' and  wccp.[Exists_in_Basic_user_Id] =1
--INNER JOIN [dbo].[src_Address] a ON a.PersNo = wccp.[Employee_Num]
--LEFT Join [dbo].[dest_PersonalArea_M] pa on pa.Rg = a.Rg
--LEFT JOIN [dbo].[dest_Address_Subtype_M] ad on ad.SubType = a.Type
--LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = a.Ctr
--LEFT JOIN [dbo].[master_picklist_state] ps on ps.[Description] = pa.[Personnel Area Text]
--WHERE a.End_Date = '12/31/9999' 

SELECT a.PersNo AS [PersonIdExternal], 
FORMAT( a.[Start_Date],'yyyy-MM-dd')  AS [EventDate],
CASE
		WHEN ad.[SubType] = 1
			THEN 'Present'
		WHEN ad.[SubType] = 2
			THEN 'Permanent'
		ELSE	
			ad.[Description]
END AS [AddressType], Upper(SUBSTRING(c.[Name], 1, 3)) AS [CountryRegion],
CASE WHEN a.House IS NOT NULL
	THEN Concat(a.House, ' , ', a.Street_and_House_Number)
	ELSE 
		 a.Street_and_House_Number
END AS [HouseNumber],
' '  AS [Street], --Street and House number has been combined
a._2nd_address_line AS [ExtraAddressLine], COALESCE(a.Postal_code, '0') AS [Pin], a.City AS [City], COALESCE(a.District, 'Not Applicable') AS [District],
pa.[Personnel Area Text],
COALESCE(ps.[Code], 'Not Applicable') AS [State]
FROM [dbo].[src_active_17_11_2021] wccp
INNER JOIN [dbo].[src_Address] a ON a.PersNo = wccp.[Employee_Num]
LEFT Join [dbo].[dest_PersonalArea_M] pa on pa.Rg = a.Rg
LEFT JOIN [dbo].[dest_Address_Subtype_M] ad on ad.SubType = a.Type  
LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = a.Ctr
LEFT JOIN [dbo].[master_picklist_state] ps on ps.[Description] = pa.[Personnel Area Text]
WHERE a.End_Date = '12/31/9999' AND a.[Type] IN (1,2) --Present and Permanent address
-----------------------------------------------------------------------------------------------------------------------------------------------


---------------------------------------------------------------------08. Payment_Info--------------------------------------------------------------------------
--SELECT * FROM [dbo].[src_Bank_Details]
--SELECT * FROM [dest_BankDetailsType_M]
--SELECT * FROM [dest_PaymentMethods_M]

--SELECT  bd.[Personnel number] AS [PaymentInformationWorker], bd.[Start Date] AS [PaymentInformationEffectiveStartDate], Upper(SUBSTRING(c.[Name], 1, 3)) AS [CountryOrRegionOrCode], 
--bdm.[Descrption] AS [PayType], bdp.[Description] AS [PaymentMethod], bd.[Bank Country Key] AS [BankCountry], bd.[Bank Keys] AS [Bank], '' AS [RoutingNumber], 
--bd.[Payee Name] AS [AccountOwner], bd.[Bank account number] AS [AccountNumber], bd.[Payment Currency] AS [Currency]
--FROM    [dbo].[src_Bank_Details] bd
--INNER JOIN [dbo].[dest_BankDetailsType_M] bdm on bdm.BankDetailsType = bd.[Bank Details Type]
--INNER JOIN [dbo].[dest_PaymentMethods_M] bdp on bdp.PaymentMethod = bd.[Payment Method] 
--LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = bd.[Bank Country Key]
--WHERE bd.[End Date] = '12/31/9999' 

--The query is little complicated due to retrival of 1481 records
--SELECT  DISTINCT  
--wccp.[Employee_Num] AS [PaymentInformationWorker], 
--	FORMAT((SELECT Top 1 sbd.[Start Date] FROM [dbo].[src_Bank_Details] sbd WHERE sbd.[Personnel number] =  wccp.[Employee_Num]  and sbd.[End Date] = '12/31/9999' ORDER BY sbd.[Start Date]  DESC),'yyyy-MM-dd') AS [PaymentInformationEffectiveStartDate]
--, (
--		SELECT Top 1 Upper(SUBSTRING(c.[Name], 1, 3)) FROM [dbo].[src_Bank_Details] crbd
--		LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = crbd.[Bank Country Key]
--		WHERE crbd.[Personnel number] =  wccp.[Employee_Num]  and crbd.[End Date] = '12/31/9999' ORDER BY crbd.[Start Date]  DESC
-- ) AS CountryOrRegionOrCode 
--,'Main Payment Method' AS [PayType], '05' AS [PaymentMethod], 
--(SELECT Top 1 bbd.[Bank Country Key] FROM [dbo].[src_Bank_Details] bbd WHERE bbd.[Personnel number] =  wccp.[Employee_Num]  and bbd.[End Date] = '12/31/9999' ORDER BY bbd.[Start Date]  DESC) AS [BankCountry]
--,baccd.[BankName] AS [Bank], '' AS [RoutingNumber], 
--baccd.[Name As Per Bank Records] AS [AccountOwner], baccd.[BankAccNo] AS [AccountNumber], 
--(SELECT Top 1 pcbd.[Payment Currency] FROM [dbo].[src_Bank_Details] pcbd WHERE pcbd.[Personnel number] =  wccp.[Employee_Num]  and pcbd.[End Date] = '12/31/9999' ORDER BY pcbd.[Start Date]  DESC) AS [Currency]
--,baccd.[IFSC Code] AS [IFSC Code]
--FROM [dbo].[src_active_17_11_2021] wccp
--LEFT JOIN [dbo].[WG01_WCC_Bank account details] baccd ON wccp.[Employee_Num]   = baccd.EmployeeNo

SELECT  DISTINCT  
wccp.[Employee_Num] AS [PaymentInformationWorker], 
'2021-11-26' AS [PaymentInformationEffectiveStartDate]
, (
		SELECT Top 1 Upper(SUBSTRING(c.[Name], 1, 3)) FROM [dbo].[src_Bank_Details] crbd
		LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = crbd.[Bank Country Key]
		WHERE crbd.[Personnel number] =  wccp.[Employee_Num]  and crbd.[End Date] = '12/31/9999' ORDER BY crbd.[Start Date]  DESC
 ) AS CountryOrRegionOrCode 
,'Main Payment Method' AS [PayType], '05' AS [PaymentMethod], 
'IN' AS [BankCountry]
,baccd.[BankName] AS [Bank], '' AS [RoutingNumber], 
baccd.[Name As Per Bank Records] AS [AccountOwner], baccd.[BankAccNo] AS [AccountNumber], 
'INR' AS [Currency]
,baccd.[IFSC Code] AS [IFSC Code]
FROM [dbo].[src_active_17_11_2021] wccp
LEFT JOIN [dbo].[WG01_WCC_Bank account details] baccd ON wccp.[Employee_Num]   = baccd.EmployeeNo
-----------------------------------------------------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------------------------------------------------




--------------------------------------------------------------10. Emergency_Contact---------------------------------------------------------------------------------
SELECT e.[Employee_Number] AS [PersonIdExternal], e.[Emergency_Contact_Name] AS [Name], ft.[Description] AS [Relationship], e.[Emergency_Contact_Number] AS [Phone], 


'' AS [EmergencyContact], '' AS [Primary]
FROM [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp
INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999' and  wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN [dbo].[src_EmergencyContactDetails] e ON e.Employee_Number = wccp.[Employee_Num]
LEFT JOIN [dbo].[src_Family_Members] fm on fm.PersNo = e.Employee_Number AND fm.End_Date = '12/31/9999' 
LEFT JOIN [dbo].[dest_TypeOfFamilyRecord_M] ft on  ft.TypeOfFamilyRecord = fm.Membr
WHERE  e.[Emergency_Contact_Name] = CONCAT(fm.First_name, ' ', fm.Last_name)
--WHERE [Employee_Number] NOT IN ( 20190513, 20190974)
-----------------------------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------11. Dependent_Info-----------------------------------------------------------------------------

SELECT FORMAT(fm.[Start_Date],'yyyy-MM-dd') AS [EventDate], fm.PersNo AS [PersonIdExternal], ft.[Description] AS [Relationship], 
'No' AS [Accompanying], 'No' AS [CopyAddressFromEmployee], 'No' AS [IsBeneficiary]
, CONCAT(fm.PersNo,'_',ROW_NUMBER() OVER(PARTITION BY fm.PersNo Order by fm.[Start_Date])) AS [RelatedPersonIdExternal], fm.First_name AS [FirstName], ' ' AS [MiddleName], fm.Last_name AS [LastName], fmg.GenderKey AS [Gender], FORMAT(fm.Birth_date,'yyyy-MM-dd')  AS [DateOfBirth]
, fm.[CoB] AS [CountryOfBirth], c.[Name] AS [Country], '' AS [NationalIDCardType], '' AS [NationalID], 0 AS [IsPrimary]
FROM [dbo].[src_active_17_11_2021] wccp
INNER JOIN [dbo].[src_Family_Members] fm ON fm.PersNo = wccp.[Employee_Num]
Inner Join [dbo].[dest_Gender_M] fmg on fmg.GenderKey = 
													CASE
														WHEN fm.Gen = 1 THEN 'M'
														WHEN fm.Gen = 2 THEN 'F'
													END
INNER JOIN [dbo].[dest_TypeOfFamilyRecord_M] ft on  ft.TypeOfFamilyRecord = fm.Membr
INNER JOIN [dbo].[dest_Country_M] c on c.ctr = fm.Nat
WHERE fm.End_Date = '12/31/9999' 
-----------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------12. Email_Info-------------------------------------------------------------------------
SELECT  c.[Personnel number] AS [PersonIdExternal], 'Business' AS [EmailType], c.[Mail Id] AS [EmailAddress], CASE WHEN c.[Mail Id] = NULL THEN 'No' ELSE 'Yes' END AS [IsPrimary]
FROM [dbo].[src_active_17_11_2021] wccp
INNER JOIN  [dbo].[src_Communication] c ON c.[Personnel number] = wccp.Employee_Num
WHERE c.SubType = '0010' and c.[End Date] = '12/31/9999'
-----------------------------------------------------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------------------------------------------------


-----------------------------------------------------------13. Employment_Info-At_Term------------------------------------------------------------------------------------
SELECT DISTINCT r.PersNo AS [UserId], r.PersNo AS [PersonIdExternal], FORMAT(r.Notice_Period_End_date,'yyyy-MM-dd') AS [SeparationDate], rrlf.ReasonForLeavingFirst AS[ReasonForExit],
rrls.ReasonForLeavingSecond AS [ReasonForExit2], r.Comments AS [Notes], FORMAT(r.Actual_Relieving_Date,'yyyy-MM-dd') AS [PayrollEndDate], 
--FORMAT(r.Actual_Relieving_Date,'yyyy-MM-dd') AS [LastDateWorked],
CASE WHEN r.Resignation_impact = 'N' THEN 'No' ELSE 'Yes' END AS [OkToRehire], '' AS [TerminationReason], FORMAT(r.Resignation_date,'yyyy-MM-dd') AS [DateOfResignation]
FROM 
[dbo].[src_Resignation_info] r
LEFT JOIN [dbo].[dest_resignation_reason_for_leaving_first_M] rrlf ON rrlf.Id = r.Reason_for_leaving_first
LEFT JOIN [dbo].[dest_resignation_reason_for_leaving_second_M] rrls ON rrls.Id = r.Reason_for_leaving_Second
Where r.End_Date = '12/31/9999' 

-----------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------14. Phone_Info----------------------------------------------------------------------------------


SELECT [PersonIdExternal], [PhoneType], [CountryRegionCode], [AreaCode], [PhoneNumber], [Extension], [IsPrimary]  FROM (
SELECT ROW_NUMBER() OVER(PARTITION BY c.[Personnel number] ORDER BY c.[Start Date] desc) AS [RowNumber],
c.[Personnel number] AS [PersonIdExternal], c.SubType AS [PhoneType], '' AS [CountryRegionCode], '' AS [AreaCode], c.[User ID/Mobile Number] AS [PhoneNumber], '' AS [Extension]
, 'No' AS [IsPrimary]
FROM    [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp
INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999' and  wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN [dbo].[src_Communication] c ON c.[Personnel number] = wccp.[Employee_Num] and c.SubType='MPHN' AND c.[End Date] = '12/31/9999' 
)temp
WHERE [RowNumber] = 1
-----------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------------------------


-----------------------------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------16. Comp_Info--------------------------------------------------------------------------------------


SELECT a.[Personnel number] AS [UserId], FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate], 
ROW_NUMBER() OVER(PARTITION BY a.[Personnel number] ORDER BY a.[Personnel number] ASC, a.[Start Date] ASC) AS [SequenceNumber], a.[Reason For Action] AS [EventReason], '' AS [PayGroup]
FROM [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp
INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999' and  wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN [dbo].[src_Actions] a ON a.[Personnel number] = wccp.Employee_Num
Where  a.[End Date] = '12/31/9999'
-----------------------------------------------------------------------------------------------------------------------------------------------



----------------------------------------------------------Position withou code text -------------------------------------------------------------------------------------

SELECT 
[PositionCode]
      ,[Employee_Num]
      ,[Employee_Name]
	  ,'A' AS effectiveStatus
	  ,'' AS effectiveStartDate
	  ,'' AS Comment
	  ,'4' AS changeReason
	  ,[Job_Role_Code]
	  ,[DesignationCode]
	  ,[CompanyCode]
      ,[BusinessUnitCode]
      ,[DivisionCode]
      ,[DepartmentCode]
	  ,[EmployeeGroup_Code]
	  ,[EmpSubGrpCode]
      ,[PA_PSA_Code]
	  ,[Cost_Centre]
	  , 'False' AS multipleIncumbentsAllowed
      , (SELECT Top 1 [PositionCode] FROM [dbo].[src_Active Profile Mapping_11_11_2021] ppc WHERE ppc.[Employee_Num] = ap.[Supervisor_ID]) AS parentPositioncode
FROM [dbo].[src_Active Profile Mapping_11_11_2021] ap

-----------------------------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------Position with Code and Text------------------------------------------------------------------------------------------

SELECT 
		[PositionCode]
      ,[Employee_Num]
      ,[Employee_Name]
	  ,'A' AS effectiveStatus
	  ,'' AS effectiveStartDate
	  ,'' AS Comment
	  ,'4' AS changeReason
	  ,[Job_Role_Code]
      ,[Role]
      ,[DesignationCode]
      ,[Designation_text]
	   ,[CompanyCode]
      ,[CompanyName]
      ,[BusinessUnitCode]
      ,[BusinessUnit]
      ,[DivisionCode]
      ,[Division]
      ,[DepartmentCode]
      ,[Department]
	  ,[EmployeeGroup_Code]
      ,[EmployeeGroup_Description]
	  ,[EmpSubGrpCode]
	  ,[EmployeeSubGrp]
      ,[PA_PSA_Code]
	  ,[PA_PSA_Name]
	  ,[Cost_Centre]
	  , 'False' AS multipleIncumbentsAllowed
	  ,[Supervisor_ID] AS parentEmpId
      ,[Supervisor_Name] AS parentEmpName
      , (SELECT Top 1 [PositionCode] FROM [dbo].[src_Active Profile Mapping_11_11_2021] ppc WHERE ppc.[Employee_Num] = ap.[Supervisor_ID]) AS parentPositioncode
FROM [dbo].[src_Active Profile Mapping_11_11_2021] ap



-----------------------------------------------------------------JOb_Info_V2_with_latest_records------------------------------------------------------------------------------

SELECT  FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate], 
		(SELECT ROW_NUMBER() OVER(PARTITION BY wccp.[Employee_Num] ORDER BY a.[Action] ASC)) AS [SeqNumber],
		wccp.Employee_Num AS [UserId], 
		wccp.PositionCode,
		am.NameOfActionType AS [Event],
		a.[Reason For Action] AS [EventReason], 
		wccp.CompanyCode AS [Company], 
		wccp.BusinessUnitCode AS [BusinessUnit], 
		wccp.DivisionCode  AS [Division], 
		wccp.[DepartmentCode] AS [Department],
		wccp.PA_PSA_Code AS [PaPsa],
		wccp.Cost_Centre AS [CostCenter],
		wccp.Supervisor_ID AS [Manager],
		wccp.Job_Role_Code as JobRole,
		wccp.DesignationCode  AS Designation,
		CASE 
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%permanent%'
				THEN 'Permanent'
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%probationer%'
				THEN 'Probationer'
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%contract%'
				THEN 'Contract'
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%interns%'
				THEN 'Intern'
			END AS [EmployeeType], 
		wccp.EmployeeGroup_Description AS [EmployeeGroup], 
		--a.[action] AS [action],
		wccp.EmployeeSubGrp AS [Grade], 	
		CASE 
				-- Action: 01	Hiring
				-- Assignment : 4	Probationer
				-- A.startdate( joining date) + 1 year -> ProbationaryPeriodEndDate
				WHEN ass.Employee_Group = '4' and a.[action] = '01'
					THEN FORMAT (DATEADD(year, 1, a.[Start Date]), 'yyyy-MM-dd')
				WHEN 
				-- Assignment : 5	Permanent
				-- Action : 05	Confirmation
				-- A.startdate(Confirmation date as) -> ProbationaryPeriodEndDate
					ass.Employee_Group = '5' and a.[action] = '5' --As of now the data given is only India  employees
					THEN FORMAT (a.[Start Date], 'yyyy-MM-dd')
				ELSE
					' ' 
		END AS [ProbationaryPeriodEndDate],
		'' AS [ContractEndDate],
		ass.Employee_Group,
		wccp.EmpSubGrpCode,
		CASE 
			-- Assignment : 4	Probationer
			WHEN ass.Employee_Group = '4' 
				THEN '1 month'
			-- Assignment : 5	Permanent
			WHEN ass.Employee_Group = '5' 
				THEN					
					CASE 
						-- ESG Legacy(old) Code : SSG & A 
						WHEN wccp.EmpSubGrpCode IN ('AE','AD','AC','AB','AA')
							THEN '4 months'
						-- ESG Legacy(old) Code : B, B1 , C , D, D1 , E 
						WHEN wccp.EmpSubGrpCode IN ('AF','AG','AH','AI','AJ','AK') 
							THEN '3 months'
						-- ESG Legacy(old) Code : 	F1, F2 
						WHEN wccp.EmpSubGrpCode IN ('AL','AM') 
							THEN '2 months'
						-- ESG Legacy(old) Code : G,G1,G2,H 
						WHEN wccp.EmpSubGrpCode IN ('AN','AO','AP','AQ') 
							THEN '1 month'
					END		 
		END AS [EmployeeNoticePeriod],
		'' AS [Notes],
		FORMAT (a.[Chngd On], 'yyyy-MM-dd') AS [ChangedDate], a.[Changed by] as [ChangedBy]
FROM [dbo].[src_active_17_11_2021] wccp
--INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999' 
INNER JOIN [dbo].[src_Actions] a ON  a.[Personnel number] = wccp.[Employee_Num] and  a.[End Date] = '12/31/9999'-- (only 999 we need probation perios end date)

--Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number  
INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]
INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason
--INNER Join [dbo].[src_employees_tagging_report] etr on etr.[EMP_NO] = a.[Personnel number] 
--LEFT JOIN [dbo].[src_Resignation_info] r on r.PersNo = a.[Personnel number]
Inner Join [dbo].[src_Assignment] ass on wccp.[Employee_Num] = ass.Personnel_number and ass.End_Date = '12/31/9999' 
--WHERE  a.[Action] IN('01') OR (a.[Action] = '60' AND a.[Reason For Action] = '03')

-----------------------------------------------------------------JOb_Info_V2_with_History_records------------------------------------------------------------------------------

SELECT  FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate], 
		(SELECT ROW_NUMBER() OVER(PARTITION BY wccp.[Employee_Num] ORDER BY a.[Action] ASC)) AS [SeqNumber],
		wccp.Employee_Num AS [UserId], 
		wccp.PositionCode,
		am.NameOfActionType AS [Event],
		a.[Reason For Action] AS [EventReason], 
		wccp.CompanyCode AS [Company], 
		wccp.BusinessUnitCode AS [BusinessUnit], 
		wccp.DivisionCode  AS [Division], 
		wccp.[DepartmentCode] AS [Department],
		wccp.PA_PSA_Code AS [PaPsa],
		wccp.Cost_Centre AS [CostCenter],
		wccp.Supervisor_ID AS [Manager], wccp.Job_Role_Code as JobRole,
		wccp.DesignationCode  AS Designation,
		CASE 
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%permanent%'
				THEN 'Permanent'
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%probationer%'
				THEN 'Probationer'
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%contract%'
				THEN 'Contract'
			WHEN lower(wccp.EmployeeGroup_Description) LIKE '%interns%'
				THEN 'Intern'
			END AS [EmployeeType], 
		wccp.EmployeeGroup_Description AS [EmployeeGroup], 
		--a.[action] AS [action],
		wccp.EmployeeSubGrp AS [Grade], 	
		CASE 
				-- Action: 01	Hiring
				-- Assignment : 4	Probationer
				-- A.startdate( joining date) + 1 year -> ProbationaryPeriodEndDate
				WHEN ass.Employee_Group = '4' and a.[action] = '01'
					THEN FORMAT (DATEADD(year, 1, a.[Start Date]), 'yyyy-MM-dd')
				WHEN 
				-- Assignment : 5	Permanent
				-- Action : 05	Confirmation
				-- A.startdate(Confirmation date as) -> ProbationaryPeriodEndDate
					ass.Employee_Group = '5' and a.[action] = '5' --As of now the data given is only India  employees
					THEN FORMAT (a.[Start Date], 'yyyy-MM-dd')
				ELSE
					' ' 
		END AS [ProbationaryPeriodEndDate],
		'' AS [ContractEndDate],
		CASE 
			-- Assignment : 4	Probationer
			WHEN ass.Employee_Group = '4' 
				THEN '1 month'
			-- Assignment : 5	Permanent
			WHEN ass.Employee_Group = '5' 
				THEN					
					CASE 
						-- ESG Legacy(old) Code : SSG & A 
						WHEN wccp.EmpSubGrpCode IN ('AE','AD','AC','AB','AA')
							THEN '4 months'
						-- ESG Legacy(old) Code : B, B1 , C , D, D1 , E 
						WHEN wccp.EmpSubGrpCode IN ('AF','AG','AH','AI','AJ','AK') 
							THEN '3 months'
						-- ESG Legacy(old) Code : 	F1, F2 
						WHEN wccp.EmpSubGrpCode IN ('AL','AM') 
							THEN '2 months'
						-- ESG Legacy(old) Code : G,G1,G2,H 
						WHEN wccp.EmpSubGrpCode IN ('AN','AO','AP','AQ') 
							THEN '1 month'
					END		 
		END AS [EmployeeNoticePeriod],
		'' AS [Notes],
		FORMAT (a.[Chngd On], 'yyyy-MM-dd') AS [ChangedDate], a.[Changed by] as [ChangedBy]
FROM [dbo].[src_active_17_11_2021] wccp
--INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999' 
INNER JOIN [dbo].[src_Actions] a ON  a.[Personnel number] = wccp.[Employee_Num] --and a.[End Date] = '12/31/9999' (only 999 we need probation perios end date)

--Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number 
INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]
INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason
--INNER Join [dbo].[src_employees_tagging_report] etr on etr.[EMP_NO] = a.[Personnel number] 
--LEFT JOIN [dbo].[src_Resignation_info] r on r.PersNo = a.[Personnel number]
Inner Join [dbo].[src_Assignment] ass on wccp.[Employee_Num] = ass.Personnel_number and ass.End_Date = '12/31/9999' 

--WHERE  a.[Action] IN('01') OR (a.[Action] = '60' AND a.[Reason For Action] = '03')

----------------------------------------------------------------------------------------------------------------------------------------------------


-----------------------------------------------------------------JOb_Info_V2_with_Hiring_records-----------------------------------------------------------------------------------
SELECT 
		FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate], 
		(SELECT ROW_NUMBER() OVER(PARTITION BY a.[Personnel number] ORDER BY a.[Start Date] ASC)) AS [SeqNumber],
		a.[Personnel number] AS [UserId], 
		concat(a.[Action],'-', am.NameOfActionType)AS [Event],--Correct
		concat(a.[Reason For Action], '-', arm.NameOfReasonForAction) AS [EventReason], --Correct
		ass.Company_Code AS [CompanyCode_H],
		ass.[Organizational_Unit] AS [Department_H],
		CONCAT(ass.Personnel_Area,'-', pa.[Personnel Area Text], '-', ass.Personnel_Subarea, '-', psa.PSubareaText) AS [PaPsa_H],
		--Job_Classification
		CONCAT(ass.Employee_Group, '-', eg.[Description]) AS [EmployeeGroup_H], 
		CONCAT(ass.Employee_Subgroup, '-', esg.[Description]) AS [Grade_H], 
		 --etr.[DESIGNATION] AS [Designation_H],
		--Employee_work_Location
		FORMAT (a.[Chngd On], 'yyyy-MM-dd') AS [ChangedDate], --Correct
		a.[Changed by] as [ChangedBy] --Correct
	FROM [dbo].[src_active_17_11_2021] wccp 
INNER JOIN [dbo].[src_Actions] a ON  a.[Personnel number] = wccp.[Employee_Num]
Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number   and ass.[Start_Date] = a.[Start Date]  
INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]
INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason
INNER JOIN [dbo].[dest_PersonalArea_M] pa on ass.Personnel_Area = pa.PA
INNER JOIN [dbo].[dest_PersonalSubarea_M] psa on ass.Personnel_Area = psa.PA and ass.Personnel_Subarea = psa.PSubarea
INNER JOIN [dbo].[dest_EmployeeGroup_M] eg on eg.EmployeeGroup = ass.Employee_Group
INNER JOIN [dbo].dest_EmployeeSubgroup_M esg on esg.EmployeeSubGroup = ass.Employee_Subgroup
WHERE  a.[Action] IN('01') OR (a.[Action] = '60' AND a.[Reason For Action] = '03')
--AND ass.End_Date = '12/31/9999'
----------------------------------------------------------------------------------------------------------------------------------------------------