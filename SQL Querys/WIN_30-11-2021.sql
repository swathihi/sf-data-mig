
------------------------------------------------------1.Basic_User_INFO-----------------------------------------------------------

SELECT DISTINCT 'Active' AS [Status], p.[Personnel_number] AS [UserId], c.[User ID/Mobile Number]AS [UserName], p.First_name AS [FirstName], 
p.Last_name AS [LastName], winp.Supervisor_ID AS [ReportingOfficer], winp.HR_Employee_Code AS [HrManager], fmg.GenderKey AS [Gender]

FROM tbl_src_Active_Profile_Mapping_WIN_19112021 winp
INNER Join [dbo].[src_Personal_data] p on winp.[Emp_No] = p.[Personnel_number] and p.End_Date = '12/31/9999'
INNER Join [dbo].[src_Communication] c on c.[Personnel number] = p.[Personnel_number] and c.SubType = '0001' and c.[End Date] = '12/31/9999'

Inner Join [dbo].[dest_Gender_M] fmg on fmg.GenderKey = 
													CASE
														WHEN p.Gender_Key = 1 THEN 'M'
														WHEN p.Gender_Key = 2 THEN 'F'
													END
--where wccp.[Exists_in_Basic_user_Id] =1
--------------------------------------------------------2.Biographical_Info-----------------------------------------------

SELECT  p.Personnel_number AS [UserId], p.Personnel_number AS [EmployeeId], 
FORMAT(p.Date_of_Birth,'yyyy-MM-dd') AS [DateOfBirth],
Upper(SUBSTRING(c.[Name], 1, 3)) AS [CountryOrRegionOfBirth], 
c.[Name] AS [CountryOfOrigin]
FROM tbl_src_Active_Profile_Mapping_WIN_19112021 winp
INNER Join [dbo].[src_Personal_data] p on winp.[EMP_NO] = p.[Personnel_number] and p.End_Date = '12/31/9999'
INNER JOIN [dbo].[dest_Country_M] c on c.Ctr = p.Nationality
--where wccp.[Exists_in_Basic_user_Id] =1

-----------------------------------------------------------3.Employment_Info-At_Hire-------------------------------------------------

SELECT [Person_ID_External], [Person_ID_External] AS [User_ID], FORMAT([Hire_Date],'yyyy-MM-dd') AS [Hire_Date], FORMAT([Hire_Date],'yyyy-MM-dd') AS [Group_Hire_Date],
[Card_Number]
FROM
(SELECT ROW_NUMBER() OVER(PARTITION BY winp.[Emp_No] ORDER BY a.[Action] ASC) AS [RowNumber],winp.[Emp_No] AS [Person_ID_External], 
a.[Start Date] AS [Hire_Date], [card].[Card] AS [Card_Number], a.[Action]
FROM [dbo].[tbl_src_Active_Profile_Mapping_WIN_19112021] winp
Inner Join [dbo].[src_Actions] a on a.[Personnel number] = winp.[Emp_No]
LEFT JOIN [dbo].[src_WEL_Access_card_details] [card] on winp.[Emp_No] = [card].[Employee_Code]
WHERE (a.[Action] = '01' OR a.[Action] = '02' OR a.[Action] = '03' OR a.[Action] = '72')
)temp WHERE temp.[RowNumber] = 1 

_-------------------------------------------------------------4.Personal-Info------------------------------------------------------

SELECT  p.Personnel_number AS Person_ID_External,
FORMAT( p.[Start_Date],'yyyy-MM-dd') AS Event_Date, p.First_Name, p.Last_name, p.Middle_Name, 
t.Title AS Salutation, ' ' AS [Display_Name], ' ' AS [Formal_Name],g.GenderKey AS Gender, COALESCE(m.MaritalStatus, 'Unknown') AS [Marital_Status], 
FORMAT( p.Valid_From_Date_of_Current_Marital_Status,'yyyy-MM-dd') AS [Marital_Status_Since], 
p.Number_of_Children, 
Upper(SUBSTRING(c.[Name], 1, 3)) AS [Nationality] , 
p.Second_Nationality, p.Mother_Tongue AS [Native_Preferred_Language], 
CASE 
	WHEN chall.CGr IS NULL
		THEN 'No'
	WHEN chall.CGr = '08'
	THEN 'No'
	ELSE
	'	Yes'
 END AS [Challenge_Status], 
--FORMAT(chall.[Start_Date],'yyyy-MM-dd') AS [Certificate_Start_Date], FORMAT(chall.End_Date,'yyyy-MM-dd') AS [Certificate_End_Date], 
NULL AS [Certificate_Start_Date], NULL AS [Certificate_End_Date],
concat(depn.FirstName, ' ', depn.LastName) AS [Father_Name],
	CASE
		WHEN  blg.[Blood Group] IS NOT NULL AND blg.[Blood Group] LIKE '%+%'
			THEN  LEFT(blg.[Blood Group], (CHARINDEX('+', blg.[Blood Group]) - 1)) + '-Positive'
		WHEN  blg.[Blood Group] IS NOT NULL AND blg.[Blood Group] LIKE '%-%'
			THEN LEFT(blg.[Blood Group], (CHARINDEX('-', blg.[Blood Group]) - 1)) + '-Negative'
	END AS [Blood Group]
FROM [dbo].[src_Personal_data] p
INNER JOIN [dbo].[tbl_src_Active_Profile_Mapping_WIN_19112021] winp ON  p.Personnel_number = winp.[EMP_NO] and p.End_Date = '9999-12-31' 
LEFT Join [dbo].[dest_Title_M] t on t.Id = p.Title 
LEFT Join [dbo].[dest_Gender_M] g on g.GenderKey = 
													CASE
														WHEN p.Gender_Key = 1 THEN 'M'
														WHEN p.Gender_Key = 2 THEN 'F'
													END
LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = p.Nationality
LEFT Join [dbo].[dest_MaritalStatus_M] m on m.Id = p.Marital_Status_Key
LEFT JOIN [dbo].[src_challenge_Info] chall on chall.PersNo = p.Personnel_number
--LEFT JOIN [dbo].[dest_Dependent_Info] depn on depn.PersonIdExternal = p.Personnel_number and depn.Relationship = 'Father' 
LEFT JOIN [dbo].[dest_BloodGroup_M] blg ON CONVERT(NVARCHAR(20),blg.Code) = p.Blood_group
--WHERE wccp.[Exists_in_Basic_user_Id] =1
order by p.Personnel_number, p.Valid_From_Date_of_Current_Marital_Status desc

------------------------------------------------------------------5.Nationality----------------------------------------------------
SELECT  winp.[EMP_NO] AS [PersonIdExternal], Upper(SUBSTRING(c.[Name], 1, 3)) AS [CountryOrRegion], 'ADN' AS [NationalIdCardType],
		an.[Adhar Number] AS [NationalId], 'Yes' AS [IsPrimary], '' AS [LinkedAdhaar]
FROM [dbo].[tbl_src_Active_Profile_Mapping_WIN_19112021] winp
INNER Join [dbo].[src_Personal_data] p on p.Personnel_number = winp.[EMP_NO] and p.End_Date = '9999-12-31' --and  winp.[Exists_in_Basic_user_Id] =1
LEFT JOIN [dbo].[src_Aadhar_Numbers] an ON an.[Personnel number] = winp.[EMP_NO] and p.End_Date = '9999-12-31'
INNER JOIN [dbo].[dest_Country_M] c on c.[Name] = 'India'

--------------------------------------------------------------------6.CF Address-------------------------------------------------------


SELECT a.PersNo AS [PersonIdExternal], 
FORMAT( a.[Start_Date],'yyyy-MM-dd')  AS [EventDate], 
CASE
		WHEN ad.[SubType] = 1
			THEN 'Present'
		WHEN ad.[SubType] = 2
			THEN 'Permanent'
		ELSE	
			ad.[Description]
END AS [AddressType], Upper(SUBSTRING(c.[Name], 1, 3)) AS [CountryRegion],
CASE WHEN a.House IS NOT NULL
	THEN Concat(a.House, ' , ', a.Street_and_House_Number)
	ELSE 
		 a.Street_and_House_Number
END AS [House No],
' '  AS [Street], a._2nd_address_line AS [ExtraAddressLine], COALESCE(a.Postal_code, '0') AS [Pin], a.City AS [City], COALESCE(a.District, 'Not Applicable') AS [District],
pa.[Personnel Area Text],
COALESCE(ps.[Code], 'Not Applicable') AS [State]
FROM [dbo].[tbl_src_Active_Profile_Mapping_WIN_19112021] winp
INNER Join [dbo].[src_Personal_data] p on winp.[EMP_NO] = p.[Personnel_number] and p.End_Date = '12/31/9999' --and  wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN [dbo].[src_Address] a ON a.PersNo = winp.[EMP_NO]
LEFT Join [dbo].[dest_PersonalArea_M] pa on pa.Rg = a.Rg
LEFT JOIN [dbo].[dest_Address_Subtype_M] ad on ad.SubType = a.Type
LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = a.Ctr
LEFT JOIN [dbo].[master_picklist_state] ps on ps.[Description] = pa.[Personnel Area Text]
WHERE a.End_Date = '12/31/9999' 

----------------------------------------------------------------7.PAYMENT INFO---------------------------------------------------------

SELECT  bd.[Personnel number] AS [PaymentInformationWorker], bd.[Start Date] AS [PaymentInformationEffectiveStartDate], Upper(SUBSTRING(c.[Name], 1, 3)) AS [CountryOrRegionOrCode], 
bdm.[Descrption] AS [PayType], bdp.[Description] AS [PaymentMethod], bd.[Bank Country Key] AS [BankCountry], bd.[Bank Keys] AS [Bank], '' AS [RoutingNumber], 
bd.[Payee Name] AS [AccountOwner], bd.[Bank account number] AS [AccountNumber], bd.[Payment Currency] AS [Currency]
FROM    [dbo].[src_Bank_Details] bd
INNER JOIN [dbo].[dest_BankDetailsType_M] bdm on bdm.BankDetailsType = bd.[Bank Details Type]
INNER JOIN [dbo].[dest_PaymentMethods_M] bdp on bdp.PaymentMethod = bd.[Payment Method] 
LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = bd.[Bank Country Key]
WHERE bd.[End Date] = '12/31/9999'

---------------------------------------------------------------8.WORK PERMIT-----------------------------------------





-------------------------------------------------------------9.EMERGENCY CONTACT-----------------------------------------
SELECT  c.[Personnel number] AS [PersonIdExternal], c.SubType AS [EmailType], c.[Mail Id] AS [EmailAddress], CASE WHEN c.[Mail Id] = NULL THEN 'No' ELSE 'Yes' END AS [IsPrimary]
FROM [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp
INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999' and  wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN  [dbo].[src_Communication] c ON c.[Personnel number] = wccp.Employee_Num
WHERE c.SubType = '0010' and c.[End Date] = '12/31/9999'
----------------------------------------------------------10.Dependent Info(Personal relationship)----------------------------------------

SELECT FORMAT(fm.[Start_Date],'yyyy-MM-dd') AS [EventDate], fm.PersNo AS [PersonIdExternal], ft.[Description] AS [Relationship], 'No' AS [Accompanying], 'No' AS [CopyAddressFromEmployee], 'No' AS [IsBeneficiary]
, CONCAT(fm.PersNo,'_',ROW_NUMBER() OVER(PARTITION BY fm.PersNo Order by fm.[Start_Date])) AS [RelatedPersonIdExternal], fm.First_name AS [FirstName], ' ' AS [MiddleName], fm.Last_name AS [LastName], fmg.GenderKey AS [Gender], FORMAT(fm.Birth_date,'yyyy-MM-dd')  AS [DateOfBirth]
, fm.[CoB] AS [CountryOfBirth], c.[Name] AS [Country], '' AS [NationalIDCardType], '' AS [NationalID], 0 AS [IsPrimary]
FROM tbl_src_Active_Profile_Mapping_WIN_19112021 winp
INNER Join [dbo].[src_Personal_data] p on winp.[EMP_NO] = p.[Personnel_number] and p.End_Date = '12/31/9999' --and  wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN [dbo].[src_Family_Members] fm ON fm.PersNo =winp.[EMP_NO]
Inner Join [dbo].[dest_Gender_M] fmg on fmg.GenderKey = 
													CASE
														WHEN fm.Gen = 1 THEN 'M'
														WHEN fm.Gen = 2 THEN 'F'
													END
INNER JOIN [dbo].[dest_TypeOfFamilyRecord_M] ft on  ft.TypeOfFamilyRecord = fm.Membr
INNER JOIN [dbo].[dest_Country_M] c on c.ctr = fm.Nat
WHERE fm.End_Date = '12/31/9999' 


--------------------------------------------------------------11.Email info------------------------------------------
SELECT  c.[Personnel number] AS [PersonIdExternal], c.SubType AS [EmailType], c.[Mail Id] AS [EmailAddress], CASE WHEN c.[Mail Id] = NULL THEN 'No' ELSE 'Yes' END AS [IsPrimary]
FROM [dbo].[tbl_src_Active_Profile_Mapping_WIN_19112021] winp
INNER Join [dbo].[src_Personal_data] p on winp.[EMP_NO] = p.[Personnel_number] and p.End_Date = '12/31/9999'--- and  wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN  [dbo].[src_Communication] c ON c.[Personnel number] = winp.EMP_NO
WHERE c.SubType = '0010' and c.[End Date] = '12/31/9999'


----------------------------------------------------------------12.Phone Info----------------------------------------------


SELECT [PersonIdExternal], [PhoneType], [CountryRegionCode], [AreaCode], [PhoneNumber], [Extension], [IsPrimary]  FROM (
SELECT ROW_NUMBER() OVER(PARTITION BY c.[Personnel number] ORDER BY c.[Start Date] desc) AS [RowNumber],
c.[Personnel number] AS [PersonIdExternal], c.SubType AS [PhoneType], '' AS [CountryRegionCode], '' AS [AreaCode], c.[User ID/Mobile Number] AS [PhoneNumber], '' AS [Extension]
, 'No' AS [IsPrimary]
FROM    [dbo].[tbl_src_Active_Profile_Mapping_WIN_19112021] winp
INNER Join [dbo].[src_Personal_data] p on winp.[EMP_NO] = p.[Personnel_number] and p.End_Date = '9999-12-31' ---and  wccp.[Exists_in_Basic_user_Id] =1
INNER JOIN [dbo].[src_Communication] c ON c.[Personnel number] = winp.[EMP_NO] and c.SubType='MPHN' AND c.[End Date] = '9999-12-31' 
)temp
WHERE [RowNumber] = 1
--------------------------------------------------------------- 13.WIN_Hiring Records------------------------------------------------------

SELECT 
		FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate], 
		(SELECT ROW_NUMBER() OVER(PARTITION BY a.[Personnel number] ORDER BY a.[Start Date] ASC)) AS [SeqNumber],
		a.[Personnel number] AS [UserId], 
		concat(a.[Action],'-', am.NameOfActionType)AS [Event],--Correct
		concat(a.[Reason For Action], '-', arm.NameOfReasonForAction) AS [EventReason], --Correct
		ass.Company_Code AS [CompanyCode_H],
		ass.[Organizational_Unit] AS [Department_H],
		CONCAT(ass.Personnel_Area,'-', pa.[Personnel Area Text], '-', ass.Personnel_Subarea, '-', psa.PSubareaText) AS [PaPsa_H],
		--Job_Classification
		CONCAT(ass.Employee_Group, '-', eg.[Description]) AS [EmployeeGroup_H], 
		CONCAT(ass.Employee_Subgroup, '-', esg.[Description]) AS [Grade_H], 
		 --etr.[DESIGNATION] AS [Designation_H],
		--Employee_work_Location
		FORMAT (a.[Chngd On], 'yyyy-MM-dd') AS [ChangedDate], --Correct
		a.[Changed by] as [ChangedBy] --Correct
	FROM [dbo].[src_active_17_11_2021] wccp 
INNER JOIN [dbo].[src_Actions] a ON  a.[Personnel number] = wccp.[Employee_Num]
Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number   and ass.[Start_Date] =a.End_Date='31/12/9999'
INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]
INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason
INNER JOIN [dbo].[dest_PersonalArea_M] pa on ass.Personnel_Area = pa.PA
INNER JOIN [dbo].[dest_PersonalSubarea_M] psa on ass.Personnel_Area = psa.PA and ass.Personnel_Subarea = psa.PSubarea
INNER JOIN [dbo].[dest_EmployeeGroup_M] eg on eg.EmployeeGroup = ass.Employee_Group
INNER JOIN [dbo].dest_EmployeeSubgroup_M esg on esg.EmployeeSubGroup = ass.Employee_Subgroup
WHERE  a.[Action] IN('01') OR (a.[Action] = '60' AND a.[Reason For Action] = '03')
AND ass.End_Date = '12/31/9999'

-------------------------------------------------------------------14.WIN History Hiring Records------------------------------------------------------------

SELECT

                                CASE

                                                WHEN a.[Action] = ('01') OR (a.[Action] = '60' AND a.[Reason For Action] = '03')

                                                                THEN '*'

                                END AS 'Hiring Record',

                                FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate],

                                (SELECT ROW_NUMBER() OVER(PARTITION BY a.[Personnel number] ORDER BY a.[Start Date] ASC)) AS [SeqNumber],

                                a.[Personnel number] AS [UserId],

                                concat(a.[Action],'-', am.NameOfActionType)AS [Event],--Correct

                                concat(a.[Reason For Action], '-', arm.NameOfReasonForAction) AS [EventReason], --Correct

                                ass.Company_Code AS [CompanyCode_H],

                                ass.[Organizational_Unit] AS [Department_H],

                                CONCAT(ass.Personnel_Area,'-', pa.[Personnel Area Text], '-', ass.Personnel_Subarea, '-', psa.PSubareaText) AS [PaPsa_H],

                                --Job_Classification

                                CONCAT(ass.Employee_Group, '-', eg.[Description]) AS [EmployeeGroup_H],

                                CONCAT(ass.Employee_Subgroup, '-', esg.[Description]) AS [Grade_H],

                                 --etr.[DESIGNATION] AS [Designation_H],

                                --Employee_work_Location

                                FORMAT (a.[Chngd On], 'yyyy-MM-dd') AS [ChangedDate], --Correct

                                a.[Changed by] as [ChangedBy] --Correct

                FROM [dbo].[tbl_src_Active_Profile_Mapping_WIN_19112021] winp

INNER JOIN [dbo].[src_Actions] a ON  a.[Personnel number] = winp.[EMP_NO]

Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number   and ass.[Start_Date] = a.[Start Date] 

INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]

INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason

LEFT JOIN [dbo].[dest_PersonalArea_M] pa on ass.Personnel_Area = pa.PA

LEFT JOIN [dbo].[dest_PersonalSubarea_M] psa on ass.Personnel_Area = psa.PA and ass.Personnel_Subarea = psa.PSubarea

INNER JOIN [dbo].[dest_EmployeeGroup_M] eg on eg.EmployeeGroup = ass.Employee_Group

INNER JOIN [dbo].dest_EmployeeSubgroup_M esg on esg.EmployeeSubGroup = ass.Employee_Subgroup

--WHERE  a.[Action] IN('01') OR (a.[Action] = '60' AND a.[Reason For Action] = '03')

--AND ass.End_Date = '12/31/9999'



------------------------------------------------------------------15.BANK PAYMENT-----------------------------------------------


SELECT [PaymentInformationWorker], [PaymentInformationEffectiveStartDate], * FROM (

SELECT ROW_NUMBER() OVER(PARTITION BY winp.[EMP_NO] ORDER BY winbaccd.[Start_Date] DESC) AS [RowNumber],

winp.[EMP_NO] AS [PaymentInformationWorker],

'2021-11-29' AS [PaymentInformationEffectiveStartDate]

, (

SELECT Top 1 Upper(SUBSTRING(c.[Name], 1, 3)) FROM [dbo].[src_Bank_Details] crbd

LEFT JOIN [dbo].[dest_Country_M] c on c.Ctr = crbd.[Bank Country Key]

WHERE crbd.[Personnel number] = winp.[EMP_NO] and crbd.[End Date] = '9999-12-31' ORDER BY crbd.[Start Date] DESC

) AS CountryOrRegionOrCode

,'Main Payment Method' AS [PayType], '05' AS [PaymentMethod],

'IN' AS [BankCountry]

,winbaccd.[Bank_Keys] AS [Bank], '' AS [RoutingNumber],

winbaccd.[Payee_Name] AS [AccountOwner], winbaccd.[Bank_account_number] AS [AccountNumber],

'INR' AS [Currency]

,winbaccd.[SWIFT_BIC_for_International_Payments] AS [IFSC Code] FROM [dbo].[tbl_src_Active_Profile_Mapping_WIN_19112021] winp

INNER JOIN [dbo].[tbl_WS3_WF01_WIN_Active emp_PA0009_Bank Details] winbaccd ON winbaccd.Personnel_number = winp.[EMP_NO] and winbaccd.[End_date]='31-12-9999'

)temp

WHERE [RowNumber] = 1 