

SELECT 
--Employee_Num, Employee_Name, Designation_text, EmployeeSubGrp, Emp_Subgrp 
 COUNT(*) AS [count] , [Job_Role_Code], [Role], [Designation_text]
 --,(SELECT Top 1 Code FROM [dbo].[master_EmployeeSubgroup] esg WHERE temp.EmployeeSubGrp = esg.Description) AS EmpSubGrpCode
FROM 
(
SELECT 
	  ass.Position AS PositionCode
	  ,ap.[Employee_Num]
      ,ap.[Employee_Name]
	  ,jd.[Job_Role_Code]
      ,ap.[Role]
	  ,(SELECT Top 1 jddes.[Designation_Code] FROM [dbo].[master_JobRole_DesignationDetails] jddes WHERE TRIM([Job_Role_Name]) = TRIM(ap.[Role]) AND 
	  (
			TRIM([Designation_Description]) = ap.[Designation_text] OR
			TRIM([Designation_Description]) = SUBSTRING(ap.[Designation_text], 1, CharIndex(ap.[Designation_text], '-')) 
			--TRIM([Designation_Description]) = SUBSTRING(ap.[Designation_text], 1, 20)
				)) AS DesignationCode
      ,ap.[Designation_text]
      ,ap.[Supervisor_ID]
      ,ap.[Supervisor_Name]
	  , CASE 
			WHEN ap.[L0] = 'Amalner Factory'
				THEN '9200'
			WHEN ap.[L0] = 'WEL Corporate'
				THEN 'WEL1'
			WHEN ap.[L0] = 'Wipro Consumer Care'
				THEN  '1000'
	   END AS CompanyCode
      ,ap.[L0] AS CompanyName
	  ,(SELECT Top 1 Business_Unit_Code FROM [dbo].[master_OrgStructureDetails] WHERE TRIM(ap.[L0]) = TRIM(CASE WHEN Company_Name_Label = 'WEL Corporate Wipro Enterprises Limited'
														THEN 'WEL Corporate' ELSE	Company_Name_Label END) AND TRIM(ap.L1) = TRIM(Business_Unit_Name_Label)) 
	  AS BusinessUnitCode
      ,ap.[L1] AS BusinessUnit
	  ,(SELECT Top 1 Division_Code FROM [dbo].[master_OrgStructureDetails] WHERE CASE WHEN ap.[L0] = 'WEL Corporate'
																						THEN 'WEL Corporate Wipro Enterprises Limited'
																					ELSE 
																						ap.[L0]
																				   END
																				   = Company_Name_Label AND 
																				   ap.L1 = Business_Unit_Name_Label AND ap.L2 = Division_Name_Label) AS DivisionCode
      ,ap.[L2] AS Division
	  ,(SELECT Top 1 Department_Code FROM [dbo].[master_OrgStructureDetails] WHERE CASE WHEN ap.[L0] = 'WEL Corporate'
																						THEN 'WEL Corporate Wipro Enterprises Limited'
																					ELSE 
																						ap.[L0]
																				   END

																					= Company_Name_Label AND
																					ap.L1 = Business_Unit_Name_Label AND ap.L2 = Division_Name_Label AND ap.L3 = Department_Name_Label) AS DepartmentCode
      ,ap.[L3] AS Department
      ,ap.[Location_Region]
	  
	  ,CONCAT(ap.[PA],'-',ap.[PSA]) AS PA_PSA_Name
	   ,(SELECT Top 1 Code_PA_PSA FROM [dbo].[master_Location] l WHERE CONCAT(ap.[PA],'-',ap.[PSA]) = Location_Name AND 
				CASE 
			WHEN ap.[L0] = 'Amalner Factory'
				THEN '9200'
			WHEN ap.[L0] = 'WEL Corporate'
				THEN 'WEL1'
			WHEN ap.[L0] = 'Wipro Consumer Care'
				THEN  '1000'
			END = CompanyCode
		) AS PA_PSA_Code
      ,ap.[PA]
      ,ap.[PSA]
      ,ap.[Cost_Centre]
	   
     ,CASE ap.[Emp_Subgrp]
		WHEN 'WCCLG Group SSG'
		   THEN  
				CASE 
					WHEN lower(ap.Designation_text) LIKE '%sr vp%' OR lower(ap.Designation_text) LIKE '%sr vice president%' OR lower(ap.Designation_text) LIKE'%chief operating officer%'					 
							OR lower(ap.Designation_text) LIKE '%senior vice president%' OR lower(ap.Designation_text) LIKE '%chro%' OR lower(ap.Designation_text) LIKE '%managing partner%'
							OR lower(ap.Designation_text) LIKE '%chief financial officer%'
						THEN 'WCC Sr VP'
					WHEN lower(ap.Designation_text) LIKE '%vice president%' 
						THEN 'WCC VP'
					WHEN lower(ap.Designation_text) LIKE '%president%' 
						THEN 'WCC President'
					WHEN lower(ap.Designation_text) LIKE '%c e o%' OR lower(ap.Designation_text) LIKE '%ceo%' OR lower(ap.Designation_text) LIKE '%chief executive%'
						THEN 'WCC CEO'
					
				END
			WHEN 'WCCLG Group D1'
				THEN 'WCC D1'
			WHEN 'WCCLG Group A'
				THEN 'WCC A'
			WHEN 'RETAINER'
				THEN 'RETAINER'
			WHEN 'WCCLG Group F1'
				THEN 'WCC F1'
			WHEN 'WCCLG Group F2'
				THEN 'WCC F2'
			WHEN 'WCCLG Group B'
				THEN 'WCC B'
			WHEN 'WCCLG Group E'
				THEN 'WCC E'
			WHEN 'WCCLG Group D'
				THEN 'WCC D'
			WHEN 'WCCLG Group C'
				THEN 'WCC C'
			WHEN 'WCCLG Group B1'
				THEN 'WCC B1'
			WHEN 'WCCLG Group G1'
				THEN 'WCC G1'
			WHEN 'WCCLG Group G'
				THEN 'WCC G'
			WHEN 'WCCLG Group H'
				THEN 'WCC H'
			WHEN 'WCCLG Group G2'
				THEN 'WCC G2'
			--WHEN 'WCCLG Trainee'
			--	THEN 
			ELSE
				ap.[Emp_Subgrp]			

		END AS EmployeeSubGrp
      ,ap.[HR_SAP_No]
      ,ap.[HR]
	  ,eg.Code AS 'EmployeeGroup_Code'
	  ,eg.[Description]  AS 'EmployeeGroup_Description'
  FROM [dbo].[src_Active Profile Mapping_2_11_2021] ap
 INNER Join [dbo].[src_wcc_personal_data_rem_emps_DNE] wccp on ap.[Employee_Num] =  wccp.Employee_Num  and wccp.[Exists_in_Basic_user_Id] =1
INNER Join [dbo].[src_Personal_data] p on wccp.[Employee_Num] = p.[Personnel_number] and p.End_Date = '12/31/9999'
  LEFT JOIN [dbo].[src_Assignment] ass on ass.Personnel_number = ap.[Employee_Num] and ass.End_Date = '12/31/9999'
  LEFT JOIN [dbo].[master_EmployeeGroup] eg on  ass.Employee_Group  = eg.[Legacy_Code]
  LEFT JOIN [dbo].[master_JobRole_DesignationDetails] jd on jd.[Job_Role_Name] = ap.[Role] 
  )temp

  WHERE 
  DesignationCode IS NULL AND [Job_Role_Code] IS NOT NULL
  GROUP BY [Job_Role_Code], [Role], [Designation_text] 
  ORDER BY [count] desc
  

