
-----------------------------------------------------------------JOb_Info_V2_with_Hiring_records-----------------------------------------------------------------------------------


SELECT 
		FORMAT(a.[Start Date],'yyyy-MM-dd') AS [EventDate], 
		(SELECT ROW_NUMBER() OVER(PARTITION BY a.[Personnel number] ORDER BY a.[Start Date] ASC)) AS [SeqNumber],
		a.[Personnel number] AS [UserId], 
		concat(a.[Action],'-', am.NameOfActionType)AS [Event],--Correct
		concat(a.[Reason For Action], '-', arm.NameOfReasonForAction) AS [EventReason], --Correct
		ass.Company_Code AS [CompanyCode_H],
		ass.[Organizational_Unit] AS [Department_H],
		CONCAT(ass.Personnel_Area,'-', pa.[Personnel Area Text], '-', ass.Personnel_Subarea, '-', psa.PSubareaText) AS [PaPsa_H],
		--Job_Classification
		CONCAT(ass.Employee_Group, '-', eg.[Description]) AS [EmployeeGroup_H], 
		CONCAT(ass.Employee_Subgroup, '-', esg.[Description]) AS [Grade_H], 
		 --etr.[DESIGNATION] AS [Designation_H],
		--Employee_work_Location
		FORMAT (a.[Chngd On], 'yyyy-MM-dd') AS [ChangedDate], --Correct
		a.[Changed by] as [ChangedBy] --Correct
	FROM [dbo].[tbl_src_Active_Profile_Mapping_WIN_19112021] wccp 
INNER JOIN [dbo].[src_Actions] a ON  a.[Personnel number] = wccp.[EMP_NO]
Inner Join [dbo].[src_Assignment] ass on a.[Personnel number] = ass.Personnel_number   and ass.[Start_Date] = a.[Start Date]  
INNER JOIN [dbo].[dest_Actions_M] am on a.[Action] = am.[Action]
INNER JOIN [dbo].[dest_ActionReasons_M] arm on a.[Action] = arm.[Action] and a.[Reason For Action] = arm.ActReason
INNER JOIN [dbo].[dest_PersonalArea_M] pa on ass.Personnel_Area = pa.PA
INNER JOIN [dbo].[dest_PersonalSubarea_M] psa on ass.Personnel_Area = psa.PA and ass.Personnel_Subarea = psa.PSubarea
INNER JOIN [dbo].[dest_EmployeeGroup_M] eg on eg.EmployeeGroup = ass.Employee_Group
INNER JOIN [dbo].dest_EmployeeSubgroup_M esg on esg.EmployeeSubGroup = ass.Employee_Subgroup
LEFT JOIN [dbo].[src_Resignation_info] r on r.PersNo = a.[Personnel number]
WHERE  a.[Action] IN('01') OR (a.[Action] = '60' AND a.[Reason For Action] = '03')
--AND ass.End_Date = '12/31/9999'


----------------------------------------------------------------------------------------------------------------------------------